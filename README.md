# ann_see

JAVA development group stands for Jérémy, Anthony, Valentin, Adrien.
Mobile application to promote the cultural heritage of the city of Annecy.

## Requirements

- Flutter SDK
- Android Studio
- ADV Manager to emulate on device

I'm really lazy to put the installation links, I'll do that later.

## Installation

- Clone the GitLab repository
- Build the project with Android Studio
- Run the project on an emulator or on a local smartphone

## Library

To realize the frontend and respect the UI trends of 2022 we used several libraries :

- [flutter_neumorphic](https://pub.dev/packages/flutter_neumorphic): ^3.2.0

## If you're new to Flutter

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
- [Online documentation](https://flutter.dev/docs)

//TODO: mettre à jour ce README
