import 'dart:async';
import 'dart:collection';
import 'package:ann_see/model/category.dart' as model;
import 'package:ann_see/screens/category/category_detail.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/game_viewmodel.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:ann_see/widgets/result_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import '../../model/activity.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:shimmer/shimmer.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _Homepage();
}

//position
Future<Position> determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}

class _Homepage extends State<Homepage> {
  Position? userPosition;
  StreamSubscription<Position>? positionStream;

  void showLikedActivities(
      model.Category category, List<Activity>? activities) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CategoryDetail(
                  category: category,
                  likedActivities: activities,
                )));
  }

  @override
  Widget build(BuildContext context) {
    final List<Marker> markers = [];

    //Gestion de la position
    determinePosition().then((position) => {}).catchError((Object error) {
      print(error.toString());
    });

    LocationSettings locationSettings = const LocationSettings(
      accuracy: LocationAccuracy.high,
      distanceFilter: 100,
    );

    positionStream =
        Geolocator.getPositionStream(locationSettings: locationSettings)
            .listen((Position? position) {
      if (position != null) {
        print("Position estimated");
        setState(() {
          userPosition = position;
        });
      }
      //print(position == null ? 'Unknown' : '${position.latitude.toString()}, ${position.longitude.toString()}');
    });

    for (String key
        in Provider.of<GameViewModel>(context, listen: false).likes.keys) {
      for (Activity activity
          in Provider.of<GameViewModel>(context, listen: false).likes[key]!) {
        markers.add(Marker(
          width: 20.0,
          height: 20.0,
          point: LatLng(
              activity.localisation.latitude, activity.localisation.longitude),
          builder: (ctx) => const Icon(
            Icons.location_pin,
            size: 35,
            color: Color.fromARGB(255, 194, 49, 49),
          ),
        ));
      }
    }

    int buttonSelected = 2;

    var numberOfLikes = Provider.of<GameViewModel>(context, listen: false)
        .likes
        .map(((String category, List<Activity?> activities) {
      return MapEntry(category, activities.length);
    }));

    int numberOfLikedCategories = 0;

    numberOfLikes.forEach((key, value) {
      if (value > 0) {
        numberOfLikedCategories += 1;
      }
    });

    final sortedLikesKeys = numberOfLikes.keys.toList(growable: false)
      ..sort((String k1, String k2) =>
          numberOfLikes[k2]!.compareTo(numberOfLikes[k1]!));
    LinkedHashMap<String, int> sortedLikes = LinkedHashMap.fromIterable(
        sortedLikesKeys.take(numberOfLikedCategories),
        key: (k) => k,
        value: (k) => numberOfLikes[k]!);

    Provider.of<GameViewModel>(context, listen: false).imagesCategory.shuffle();

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.fromLTRB(40, 15, 40, 5),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                child: Text(
                  "Carte interactive",
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xCC222831),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Rubik"),
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                  child: Neumorphic(
                    style: const NeumorphicStyle(
                        border: NeumorphicBorder(
                            width: 2, color: Color(0xFFFFFFFF)),
                        color: Color(0xFFEEEEEE),
                        lightSource: LightSource.topLeft,
                        shadowDarkColor: Color(0xBFAAAACC),
                        shadowLightColor: Color(0xFFFFFFFF),
                        intensity: 3,
                        depth: 3),
                    child: FlutterMap(
                      options: MapOptions(
                        plugins: [
                          const LocationMarkerPlugin(), // <-- add plugin here
                        ],
                        center: LatLng(45.899247, 6.129384),
                        zoom: 15,
                      ),
                      layers: [
                        TileLayerOptions(
                          urlTemplate:
                              "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                          subdomains: ['a', 'b', 'c'],
                        ),
                        LocationMarkerLayerOptions(),
                        MarkerLayerOptions(
                          markers: markers,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                child: Text(
                  "🏆 Tous vos résultats",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(0xCC222831),
                      fontWeight: FontWeight.w200,
                      fontFamily: "Avenir Next"),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 3,
                child: numberOfLikedCategories > 0
                    ? ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount:
                            sortedLikes.length <= 4 ? sortedLikes.length : 4,
                        itemBuilder: (context, index) {
                          return FutureBuilder<DocumentSnapshot>(
                            future: Provider.of<CategoryViewModel>(context,
                                    listen: true)
                                .get(uid: sortedLikes.keys.elementAt(index)),
                            builder: (BuildContext context,
                                AsyncSnapshot<DocumentSnapshot> snapshot) {
                              if (snapshot.hasError) {
                                return const Text("Something went wrong");
                              }

                              if (snapshot.hasData && !snapshot.data!.exists) {
                                return const Text("Document does not exist");
                              }

                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                model.Category category =
                                    snapshot.data!.data()! as model.Category;

                                return ResultCard(
                                    rank: index + 1,
                                    image: category.url,
                                    category: category.label,
                                    onPressed: () => showLikedActivities(
                                        category,
                                        Provider.of<GameViewModel>(context,
                                                listen: false)
                                            .likes[snapshot.data!.id]));
                              }

                              return Container(
                                  height: 120.0,
                                  width: 75.0,
                                  padding: const EdgeInsets.only(
                                      top: 10.0,
                                      left: 10,
                                      right: 10.0,
                                      bottom: 15.0),
                                  color: Colors.transparent,
                                  child: Shimmer.fromColors(
                                    period: const Duration(milliseconds: 500),
                                    baseColor: const Color(0xFFEEEEEE),
                                    highlightColor: const Color(0xFF3FC1C9),
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: Color(0xFF3FC1C9),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                      ),
                                      child: const Center(
                                        child: Text(
                                          "",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 22),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  )); //loading categorie;
                            },
                          );
                        },
                      )
                    : const Padding(
                        padding: EdgeInsets.fromLTRB(0, 90, 0, 30),
                        child: Text(
                          "Likez des activités pour m'afficher !",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 30,
                              color: Color(0xCC222831),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Avenir Next"),
                        ),
                      ),
              ),
              const ann_see.BottomNavigationBar(),
            ],
          ),
        ),
      ),
    );
  }
}
