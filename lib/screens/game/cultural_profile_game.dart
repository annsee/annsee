import 'package:ann_see/screens/homepage/homepage.dart';
import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart' as model;
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/game_viewmodel.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:provider/provider.dart';
import 'package:ann_see/viewmodel/activity_viewmodel.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../widgets/buttons/game_button.dart';
import 'package:shimmer/shimmer.dart';

class CulturalProfileGame extends StatefulWidget {
  const CulturalProfileGame({Key? key}) : super(key: key);

  @override
  State<CulturalProfileGame> createState() => _CulturalProfileGameState();
}

class _CulturalProfileGameState extends State<CulturalProfileGame> {
  final Stream<QuerySnapshot> _activitiesStream = FirebaseFirestore.instance
      .collection('activities')
      .withConverter<Activity>(
          fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
          toFirestore: (activity, _) => activity.toJson())
      .snapshots();

  void likeEvent() async {
    var _currentActivity =
        Provider.of<ActivityViewModel>(context, listen: false).getActivities()![
            Provider.of<GameViewModel>(context, listen: false).gameCount - 1];

    await FirebaseAnalytics.instance.logSelectContent(
      contentType: "Activity",
      itemId: _currentActivity.id.toString(),
    );

    await FirebaseAnalytics.instance
        .logSelectItem(itemListId: _currentActivity.id.toString());

    if (Provider.of<GameViewModel>(context, listen: false).gameCount <
        Provider.of<ActivityViewModel>(context, listen: false)
            .getActivities()!
            .length) {
      setState(() {
        Provider.of<GameViewModel>(context, listen: false)
            .likes[_currentActivity.category.id]!
            .add(_currentActivity);
        Provider.of<GameViewModel>(context, listen: false).gameCount += 1;
      });
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Homepage()),
      );
    }
  }

  int buttonSelected = 1;

  void dislikeEvent() {
    if (Provider.of<GameViewModel>(context, listen: false).gameCount <
        Provider.of<ActivityViewModel>(context, listen: false)
            .getActivities()!
            .length) {
      setState(() {
        Provider.of<GameViewModel>(context, listen: false).gameCount += 1;
      });
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Homepage()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            padding: const EdgeInsets.fromLTRB(40, 20, 40, 5),
            child: StreamBuilder<QuerySnapshot>(
                stream: _activitiesStream,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container(
                        height: 20.0,
                        width: 75.0,
                        color: Colors.transparent,
                        child: Shimmer.fromColors(
                          period: const Duration(milliseconds: 500),
                          baseColor: const Color(0xFFEEEEEE),
                          highlightColor: const Color(0xFF3FC1C9),
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Color(0xFF3FC1C9),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: const Center(
                              child: Text(
                                "",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 22),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        )); //loading activities
                  }

                  if (Provider.of<ActivityViewModel>(context, listen: true)
                          .getActivities() ==
                      null) {
                    Provider.of<ActivityViewModel>(context, listen: true)
                        .setActivities(snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                          Activity activity = document.data()! as Activity;

                          return activity;
                        }).toList())!
                        .shuffle();
                  }

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Event picture component
                      Neumorphic(
                        style: const NeumorphicStyle(
                            border: NeumorphicBorder(
                                color: Color(0xFFFFFFFF), width: 5),
                            boxShape: NeumorphicBoxShape.circle(),
                            lightSource: LightSource.topLeft,
                            shadowDarkColor: Color(0xBFAAAACC),
                            shadowLightColor: Color(0xFFFFFFFF),
                            shadowLightColorEmboss: Color(0xFFFFFFFF),
                            shadowDarkColorEmboss: Color(0xBFAAAACC),
                            intensity: 3,
                            depth: 3),
                        child: SizedBox(
                          width: 300,
                          height: 300,
                          child: CachedNetworkImage(
                              imageUrl: Provider.of<ActivityViewModel>(context,
                                      listen: false)
                                  .getActivities()![Provider.of<GameViewModel>(
                                              context,
                                              listen: true)
                                          .gameCount -
                                      1]
                                  .url,
                              placeholder: (context, url) =>
                                  Image.memory(kTransparentImage),
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error, size: 200),
                              fit: BoxFit.fill,
                              width: 300,
                              height: 300),
                        ),
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    Provider.of<ActivityViewModel>(context,
                                            listen: true)
                                        .getActivities()![
                                            Provider.of<GameViewModel>(context,
                                                        listen: true)
                                                    .gameCount -
                                                1]
                                        .label,
                                    style: const TextStyle(
                                        fontSize: 28,
                                        color: Color(0xFF222831),
                                        fontWeight: FontWeight.w700,
                                        fontFamily: "Rubik"),
                                  ),
                                  FutureBuilder<DocumentSnapshot>(
                                    future: Provider.of<CategoryViewModel>(
                                            context,
                                            listen: true)
                                        .get(
                                            uid: Provider.of<ActivityViewModel>(
                                                    context,
                                                    listen: true)
                                                .getActivities()![
                                                    Provider.of<GameViewModel>(
                                                                context,
                                                                listen: true)
                                                            .gameCount -
                                                        1]
                                                .category
                                                .id),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<DocumentSnapshot>
                                            snapshot) {
                                      if (snapshot.hasError) {
                                        return const Text(
                                            "Something went wrong");
                                      }

                                      if (snapshot.hasData &&
                                          !snapshot.data!.exists) {
                                        return const Text(
                                            "Document does not exist");
                                      }

                                      if (snapshot.connectionState ==
                                          ConnectionState.done) {
                                        model.Category category = snapshot.data!
                                            .data()! as model.Category;

                                        return Text(category.label,
                                            style: const TextStyle(
                                                fontSize: 14.0,
                                                color: Color(0x80222831),
                                                fontWeight: FontWeight.w200,
                                                fontFamily: "Avenir Next"));
                                      }

                                      return Container(
                                          height: 20.0,
                                          width: 75.0,
                                          color: Colors.transparent,
                                          child: Shimmer.fromColors(
                                            period: const Duration(
                                                milliseconds: 500),
                                            baseColor: const Color(0xFFEEEEEE),
                                            highlightColor:
                                                const Color(0xFF3FC1C9),
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                color: Color(0xFF3FC1C9),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10.0)),
                                              ),
                                              child: const Center(
                                                child: Text(
                                                  "",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 22),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          )); //loading categorie
                                    },
                                  )
                                ]),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              Provider.of<ActivityViewModel>(context,
                                      listen: false)
                                  .getActivities()![Provider.of<GameViewModel>(
                                              context,
                                              listen: false)
                                          .gameCount -
                                      1]
                                  .description,
                              textAlign: TextAlign.justify,
                              style: const TextStyle(
                                  fontSize: 16,
                                  color: Color(0xCC222831),
                                  fontWeight: FontWeight.w200,
                                  fontFamily: "Avenir Next"),
                            ),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GameButton(
                            onPressed: dislikeEvent,
                            iconData: Icons.heart_broken,
                            color: const Color(0xFF3FC1C9),
                          ),
                          GameButton(
                              onPressed: likeEvent,
                              iconData: Icons.favorite,
                              color: const Color(0xFFFF75A0)),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: NeumorphicProgress(
                              style: const ProgressStyle(
                                  border: NeumorphicBorder(
                                      color: Color(0xFFFFFFFF), width: 1),
                                  lightSource: LightSource.topLeft,
                                  depth: 3),
                              percent: Provider.of<GameViewModel>(context,
                                          listen: false)
                                      .gameCount /
                                  Provider.of<ActivityViewModel>(context,
                                          listen: false)
                                      .getActivities()!
                                      .length,
                            ),
                          )
                        ],
                      ),
                      const ann_see.BottomNavigationBar(),
                    ],
                  );
                })),
      ),
    );
  }
}

void blank() {}
