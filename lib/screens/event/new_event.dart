import 'dart:math';

import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/screens/event/event_list.dart';
import 'package:ann_see/viewmodel/activity_viewmodel.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/widgets/profile/profile_button.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:ann_see/widgets/utils/blurry_calendar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:intl/intl.dart';
import 'package:ann_see/model/category.dart' as model;
import 'package:maps_launcher/maps_launcher.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;

FirebaseAuth auth = FirebaseAuth.instance;

class NewEvent extends StatefulWidget {
  final bool backArrow;

  const NewEvent({Key? key, required this.backArrow}) : super(key: key);

  @override
  State<NewEvent> createState() => _NewEvent();
}

class _NewEvent extends State<NewEvent> {
  // String _selectedDate = '';
  // String _dateCount = '';
  // String _range = '';
  // String _rangeCount = '';
  bool hasDate = false;
  var imageLinkController =
      TextEditingController(text: "https://placekitten.com/1920/1080");
  var tempImageLinkController = TextEditingController();
  var nameController = TextEditingController();
  var descriptionController = TextEditingController();
  var dateController = TextEditingController();
  var adressController = TextEditingController();
  bool isCreationLoading = false;

  model.Category? _dropdownValue;

  final Stream<QuerySnapshot> _categoriesStream = FirebaseFirestore.instance
      .collection('categories')
      .withConverter<model.Category>(
          fromFirestore: (snapshot, _) =>
              model.Category.fromJson(snapshot.data()!),
          toFirestore: (category, _) => category.toJson())
      .snapshots();

  // void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
  //   setState(() {
  //     if (args.value is PickerDateRange) {
  //       _range = '${DateFormat('dd/MM/yyyy').format(args.value.startDate)} -'
  //           // ignore: lines_longer_than_80_chars
  //           ' ${DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate)}';
  //     } else if (args.value is DateTime) {
  //       _selectedDate = args.value.toString();
  //     } else if (args.value is List<DateTime>) {
  //       _dateCount = args.value.length.toString();
  //     } else {
  //       _rangeCount = args.value.length.toString();
  //     }
  //   });
  // }

  bool isFormValid() {
    return nameController.text.toString().trim() != "" &&
            descriptionController.text.toString().trim() != "" &&
            adressController.text.toString().trim() != "" &&
            _dropdownValue != null &&
            hasDate == false
        ? true
        : dateController.text.toString().trim() != "";
  }

  void uploadEventImage() {
    //TODO: Implements upload image method
  }

  // void setEventDate() {
  //   VoidCallback continueCallBack = () => {
  //         //TODO: Récupérer la date choisie par le user et l'envoyer à Firestore
  //       };

  // BlurryCalendar alert =
  //     BlurryCalendar(_onSelectionChanged, continueCallBack);

  // showDialog(
  //   context: context,
  //   builder: (BuildContext context) {
  //     return alert;
  //   },
  // );
  // }

  void dropDownCallback(model.Category? selectedValue) {
    if (selectedValue is model.Category) {
      setState(() {
        _dropdownValue = selectedValue;
      });
    }
  }

  void createEvent() async {
    if (isFormValid()) {
      setState(() {
        isCreationLoading = true;
      });
      var categoryInstance = FirebaseFirestore.instance
          .collection("categories")
          .withConverter<model.Category>(
              fromFirestore: (snapshot, _) =>
                  model.Category.fromJson(snapshot.data()!),
              toFirestore: (category, _) => category.toJson());

      var categoryDocument = await categoryInstance
          .where("id", isEqualTo: _dropdownValue!.id)
          .get();

      var activities = await FirebaseFirestore.instance
          .collection("activities")
          .withConverter<Activity>(
              fromFirestore: (snapshot, _) =>
                  Activity.fromJson(snapshot.data()!),
              toFirestore: (activity, _) => activity.toJson())
          .get();

      var activitiesSize = activities.size;

      var categoryRef = categoryInstance.doc(categoryDocument.docs.first.id);

      var organizerRef = FirebaseFirestore.instance
          .collection("organizers")
          .withConverter<Organizer>(
              fromFirestore: (snapshot, _) =>
                  Organizer.fromJson(snapshot.data()!),
              toFirestore: (organizer, _) => organizer.toJson())
          .doc(FirebaseAuth.instance.currentUser!.uid);

      var date = DateTime.tryParse(dateController.text.trim().toString());

      // From a query
      List<Location> locations =
          await locationFromAddress(adressController.text.trim());

      var coordinates = locations.first;

      Provider.of<ActivityViewModel>(context, listen: false)
          .create(
              id: activitiesSize + 1,
              category: categoryRef,
              label: nameController.text.trim().toString(),
              description: descriptionController.text.trim().toString(),
              date: date != null ? Timestamp.fromDate(date) : Timestamp.now(),
              limiteAge: 0,
              nbPersonnes: 1,
              prix: 0,
              localisation:
                  GeoPoint(coordinates.latitude, coordinates.longitude),
              url: imageLinkController.text.trim().toString(),
              organizer: organizerRef)
          .then((updated) {
        if (updated) {
          setState(() {
            Fluttertoast.showToast(
              msg: "Évènement créé !",
              backgroundColor: const Color(0xFF3FC1C9),
              timeInSecForIosWeb: 3,
            );
            isCreationLoading = false;
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const EventList()));
          });
        }
      });
    } else {
      Fluttertoast.showToast(
          msg: "Veuillez remplir tous les champs",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            clipBehavior: Clip.hardEdge,
            children: [
              ShaderMask(
                shaderCallback: (rect) {
                  return const LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.black, Colors.transparent],
                  ).createShader(Rect.fromLTRB(
                      0, rect.height * 0.8, rect.width, rect.height));
                },
                blendMode: BlendMode.dstIn,
                child: Image.network(
                  imageLinkController.text,
                  height: MediaQuery.of(context).size.height / pi,
                  fit: BoxFit.cover,
                ),
              ),
              widget.backArrow
                  ? const Padding(
                      padding: EdgeInsets.all(40),
                      child: BackButton(
                        color: Color(0xFFEEEEEE),
                      ),
                    )
                  : const SizedBox.shrink(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 150, 40, 0),
                    child: ProfileButton(
                        iconData: Icons.add,
                        color: const Color(0xFF00ADB5),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  content: Stack(
                                    clipBehavior: Clip.antiAlias,
                                    children: <Widget>[
                                      Positioned(
                                        right: -40.0,
                                        top: -40.0,
                                        child: InkResponse(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const CircleAvatar(
                                            backgroundColor: Colors.red,
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: TextInputField(
                                              controller:
                                                  tempImageLinkController,
                                              hintText: "Lien vers l'image",
                                              obscureText: false,
                                              inputType: TextInputType.url,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: NeumorphicButton(
                                                onPressed: () {
                                                  if (tempImageLinkController
                                                          .text !=
                                                      "") {
                                                    setState(() {
                                                      imageLinkController.text =
                                                          tempImageLinkController
                                                              .text;
                                                    });
                                                    Navigator.pop(context);
                                                  } else {
                                                    Fluttertoast.showToast(
                                                        msg:
                                                            "Veuillez entrer un lien vers l'image",
                                                        toastLength:
                                                            Toast.LENGTH_LONG,
                                                        gravity:
                                                            ToastGravity.BOTTOM,
                                                        timeInSecForIosWeb: 3,
                                                        backgroundColor:
                                                            Colors.red,
                                                        textColor: Colors.white,
                                                        fontSize: 16.0);
                                                  }
                                                },
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        30, 20, 30, 20),
                                                style: NeumorphicStyle(
                                                    shape: NeumorphicShape.flat,
                                                    boxShape: NeumorphicBoxShape
                                                        .roundRect(BorderRadius
                                                            .circular(25)),
                                                    color:
                                                        const Color(0xFF00ADB5),
                                                    lightSource:
                                                        LightSource.bottomRight,
                                                    shadowDarkColor:
                                                        const Color(0xBFAAAACC),
                                                    shadowLightColor:
                                                        const Color(0xFFFFFFFF),
                                                    intensity: 5,
                                                    depth: 5),
                                                child: const Text(
                                                  "Modifier l'image",
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Color(0xFFEEEEEE),
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      letterSpacing: 0.5,
                                                      fontFamily:
                                                          "Avenir Next"),
                                                )),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              });
                        }),
                  ),
                ],
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 20, 40, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextInputField(
                    controller: nameController,
                    hintText: "Nom de l'événement",
                    obscureText: false),
                TextInputField(
                  controller: descriptionController,
                  hintText: "Description de l'événement",
                  obscureText: false,
                  inputType: TextInputType.multiline,
                  multilineRows: 5,
                ),
                TextInputField(
                  controller: adressController,
                  hintText: "Adresse (Rue, Ville, Code postal)",
                  obscureText: false,
                  inputType: TextInputType.streetAddress,
                  multilineRows: 2,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextInputField(
                          controller: dateController,
                          hintText: "Date de l'évènement",
                          obscureText: false,
                          inputType: TextInputType.datetime,
                          enabled: hasDate,
                        ),
                      ),

                      /// https://pub.dev/packages/syncfusion_flutter_datepicker/example
                      // ProfileButton(
                      //     iconData: Icons.calendar_today_rounded,
                      //     color: const Color(0xFF00ADB5),
                      //     onPressed: setEventDate)
                      NeumorphicSwitch(
                          onChanged: (value) => setState(() {
                                hasDate = value;
                                if (dateController.text != "") {
                                  dateController.text = "";
                                }
                              }),
                          value: hasDate,
                          style: const NeumorphicSwitchStyle(
                            activeTrackColor: Color(0xFF00ADB5),
                            thumbShape: NeumorphicShape.flat,
                            lightSource: LightSource.topRight,
                          ))
                    ],
                  ),
                ),

                // child: Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   children: [
                //     TextInputField(
                //       controller: TextEditingController(),
                //       hintText: "Date de l'évènement",
                //       obscureText: false,
                //       inputType: TextInputType.datetime,
                //     ),

                //     /// https://pub.dev/packages/syncfusion_flutter_datepicker/example
                //     ProfileButton(
                //         iconData: Icons.calendar_today_rounded,
                //         color: const Color(0xFF00ADB5),
                //         onPressed: setEventDate)
                //   ],
                // ),

                Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: StreamBuilder<QuerySnapshot>(
                        stream: _categoriesStream,
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasError) {
                            return const Text('Something went wrong');
                          }

                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Text("Loading...");
                          }

                          if (Provider.of<CategoryViewModel>(context,
                                      listen: true)
                                  .getCategories() ==
                              null) {
                            Provider.of<CategoryViewModel>(context,
                                    listen: true)
                                .setCategories(snapshot.data!.docs
                                    .map((DocumentSnapshot document) {
                              model.Category category =
                                  document.data()! as model.Category;

                              return category;
                            }).toList());
                          }

                          return DropdownButton<model.Category>(
                            items: Provider.of<CategoryViewModel>(context,
                                    listen: true)
                                .getCategories()!
                                .map((category) =>
                                    DropdownMenuItem<model.Category>(
                                        value: category,
                                        child: Text(category.label)))
                                .toList(),
                            value: _dropdownValue,
                            onChanged: dropDownCallback,
                            icon: const Icon(Icons.arrow_drop_down),
                            elevation: 16,
                            underline: Container(
                              height: 1,
                              color: const Color(0xFF00ADB5),
                            ),
                          );
                        })),
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: isCreationLoading
                      ? const SpinKitFadingCircle(
                          color: Colors.blueGrey,
                          size: 50.0,
                        ) // Loading spinner
                      : NeumorphicButton(
                          onPressed: createEvent,
                          padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                          style: NeumorphicStyle(
                              shape: NeumorphicShape.flat,
                              boxShape: NeumorphicBoxShape.roundRect(
                                  BorderRadius.circular(25)),
                              color: const Color(0xFF00ADB5),
                              lightSource: LightSource.bottomRight,
                              shadowDarkColor: const Color(0xBFAAAACC),
                              shadowLightColor: const Color(0xFFFFFFFF),
                              intensity: 5,
                              depth: 5),
                          child: const Text(
                            "Créer l'événement",
                            style: TextStyle(
                                fontSize: 16,
                                color: Color(0xFFEEEEEE),
                                fontWeight: FontWeight.w800,
                                letterSpacing: 0.5,
                                fontFamily: "Avenir Next"),
                          )),
                ),
              ],
            ),
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(40, 75, 40,
                  0), // TODO Fix the whole page because its not right
              child: const ann_see.BottomNavigationBar())
        ],
      ),
    );
  }
}
