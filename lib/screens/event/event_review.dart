import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart';
import 'package:ann_see/model/review.dart';
import 'package:ann_see/model/user.dart' as model;
import 'package:ann_see/widgets/event/review/event_review_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shimmer/shimmer.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class EventReview extends StatefulWidget {
  final Category category;
  final Activity activity;

  const EventReview({Key? key, required this.category, required this.activity})
      : super(key: key);

  @override
  State<EventReview> createState() => _EventReview();
}

class _EventReview extends State<EventReview> {
  @override
  Widget build(BuildContext context) {
    List<dynamic> getMany(dynamic documentRefs) {
      return documentRefs
          .map((docRef) => (docRef as DocumentReference)
              .withConverter(
                  fromFirestore: (snapshot, _) =>
                      Review.fromJson(snapshot.data()!),
                  toFirestore: (review, _) => (review as Review).toJson())
              .snapshots())
          .toList();
    }

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: BackButton(
                      color: Color(0xE6222831),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.activity.label,
                          style: const TextStyle(
                              fontSize: 24,
                              color: Color(0xE6222831),
                              fontWeight: FontWeight.w700,
                              fontFamily: "Avenir Next"),
                        ),
                        Text(
                          widget.category.label,
                          style: const TextStyle(
                              fontSize: 16,
                              color: Color(0x80222831),
                              fontWeight: FontWeight.w200,
                              fontFamily: "Avenir Next"),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 4),
                  scrollDirection: Axis.vertical,
                  child: Column(
                      children: widget.activity.reviews != null
                          ? getMany(widget.activity.reviews).map((snapshot) {
                              return StreamBuilder<DocumentSnapshot>(
                                  stream: snapshot as Stream<DocumentSnapshot>,
                                  builder: (BuildContext context,
                                      AsyncSnapshot<DocumentSnapshot> query) {
                                    if (query.hasError) {
                                      return const Text('Something went wrong');
                                    }

                                    if (query.hasData &&
                                        query.connectionState ==
                                            ConnectionState.active) {
                                      Review review =
                                          query.data!.data() as Review;

                                      return StreamBuilder<DocumentSnapshot>(
                                          stream: review.user
                                              .withConverter(
                                                  fromFirestore:
                                                      (fireStoreSnapshot, _) =>
                                                          model.User.fromJson(
                                                              fireStoreSnapshot
                                                                  .data()!),
                                                  toFirestore: (user, _) =>
                                                      (user as model.User)
                                                          .toJson())
                                              .snapshots(),
                                          builder: (BuildContext context,
                                              AsyncSnapshot<DocumentSnapshot>
                                                  localSnapshot) {
                                            if (localSnapshot.hasError) {
                                              return const Text(
                                                  "Something went wrong");
                                            }

                                            if (localSnapshot.hasData &&
                                                localSnapshot.connectionState ==
                                                    ConnectionState.active) {
                                              model.User user =
                                                  localSnapshot.data!.data()
                                                      as model.User;

                                              return EventReviewCard(
                                                userPseudo: user.nom,
                                                userReview: review.comment,
                                                userNote:
                                                    review.note.toDouble(),
                                              );
                                            }

                                            return Align(
                                                child: Container(
                                                    height: 120.0,
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 10.0,
                                                            left: 10,
                                                            right: 10.0,
                                                            bottom: 10.0),
                                                    color: Colors.transparent,
                                                    child: Shimmer.fromColors(
                                                      period: const Duration(
                                                          milliseconds: 500),
                                                      baseColor: const Color(
                                                          0xFFEEEEEE),
                                                      highlightColor:
                                                          const Color(
                                                              0xFF3FC1C9),
                                                      child: Container(
                                                        decoration:
                                                            const BoxDecoration(
                                                          color:
                                                              Color(0xFF3FC1C9),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10.0)),
                                                        ),
                                                        child: const Center(
                                                          child: Text(
                                                            "",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 22),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                        ),
                                                      ),
                                                    )));
                                          });
                                    }

                                    return Align(
                                        child: Container(
                                            height: 120.0,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.5,
                                            padding: const EdgeInsets.only(
                                                top: 10.0,
                                                left: 10,
                                                right: 10.0,
                                                bottom: 10.0),
                                            color: Colors.transparent,
                                            child: Shimmer.fromColors(
                                              period: const Duration(
                                                  milliseconds: 500),
                                              baseColor:
                                                  const Color(0xFFEEEEEE),
                                              highlightColor:
                                                  const Color(0xFF3FC1C9),
                                              child: Container(
                                                decoration: const BoxDecoration(
                                                  color: Color(0xFF3FC1C9),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0)),
                                                ),
                                                child: const Center(
                                                  child: Text(
                                                    "",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 22),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                            )));
                                  });
                            }).toList()
                          : [Text("Aucun avis n'a été posté !")]

                      // const [
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      //   EventReviewCard(
                      //       userPseudo: "Jeannine",
                      //       userReview:
                      //           "Un putain d'évènement sa mère c'était le feu j'ai juré !"),
                      // ],
                      )),
            )
          ]),
        ),
      ),
    );
  }
}
