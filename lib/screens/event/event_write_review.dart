import 'package:ann_see/screens/event/event_detail.dart';
import 'package:ann_see/viewmodel/activity_viewmodel.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/review_viewmodel.dart';
import 'package:ann_see/widgets/buttons/authentication_button.dart';
import 'package:ann_see/widgets/event/review/event_review_card.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../../model/activity.dart';
import '../../model/category.dart';
import '../../model/user.dart' as model;

FirebaseAuth auth = FirebaseAuth.instance;

class EventWriteReview extends StatefulWidget {
  final Category category;
  final Activity activity;

  const EventWriteReview(
      {Key? key, required this.category, required this.activity})
      : super(key: key);

  @override
  State<EventWriteReview> createState() => _EventWriteReview();
}

class _EventWriteReview extends State<EventWriteReview> {
  @override
  Widget build(BuildContext context) {
    var commentController = TextEditingController();

    var usersCollectionReference = FirebaseFirestore.instance
        .collection("users")
        .withConverter<model.User>(
            fromFirestore: (snapshot, _) =>
                model.User.fromJson(snapshot.data()!),
            toFirestore: (user, _) => user.toJson());

    var activitiesCollectionReference = FirebaseFirestore.instance
        .collection("activities")
        .withConverter<Activity>(
            fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
            toFirestore: (activity, _) => activity.toJson());

    return Scaffold(
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Stack(
        children: [
          ShaderMask(
            shaderCallback: (rect) {
              return const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.black, Colors.transparent],
              ).createShader(
                  Rect.fromLTRB(0, rect.height * 0.8, rect.width, rect.height));
            },
            blendMode: BlendMode.dstIn,
            child: Image.network(
              widget.activity.url,
              height: MediaQuery.of(context).size.height / 1.8,
              fit: BoxFit.cover,
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(40),
            child: BackButton(
              color: Color(0xFFEEEEEE),
            ),
          ),
        ],
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(40, 30, 0, 5),
        child: Text(
          widget.activity.label,
          style: const TextStyle(
              fontSize: 22,
              color: Color(0xE6222831),
              fontWeight: FontWeight.w700,
              fontFamily: "Avenir Next"),
        ),
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(40, 0, 0, 20),
        child: Text(
          widget.category.label,
          style: const TextStyle(
              fontSize: 14,
              color: Color(0x80222831),
              fontWeight: FontWeight.w200,
              fontFamily: "Avenir Next"),
        ),
      ),
      // 32 pixel instead of usual 40 because the widget already has little margin
      Padding(
        padding: const EdgeInsets.fromLTRB(32, 0, 40, 20),
        child: RatingBar.builder(
          initialRating: 3,
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemSize: 25,
          itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => const Icon(
            Icons.star,
            color: Color(0xFFFCE38A),
          ),
          onRatingUpdate: (rating) {
            print(rating);
          },
        ),
      ),
      Padding(
          padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
          child: Neumorphic(
            style: NeumorphicStyle(
                shape: NeumorphicShape.concave,
                boxShape:
                    NeumorphicBoxShape.roundRect(BorderRadius.circular(25)),
                color: const Color(0xFFEEEEEE),
                lightSource: LightSource.topLeft,
                shadowLightColorEmboss: const Color(0xFFFFFFFF),
                shadowDarkColorEmboss: const Color(0xBFAAAACC),
                intensity: 3,
                depth: -3),
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: TextField(
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              controller: commentController,
              obscureText: false,
              textAlign: TextAlign.start,
              decoration: const InputDecoration(
                  hintText: "Que pensez-vous de cette activité ? 😊",
                  hintStyle: TextStyle(color: Color(0x4D222831)),
                  contentPadding: EdgeInsets.symmetric(horizontal: 20),
                  border: InputBorder.none),
              style: const TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w200,
                  fontFamily: "Avenir Next"),
            ),
          )),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          NeumorphicButton(
              onPressed: () {
                Provider.of<ReviewViewModel>(context, listen: false)
                    .create(
                        note: 3,
                        comment: commentController.text.trim().toString(),
                        user: usersCollectionReference
                            .doc(FirebaseAuth.instance.currentUser!.uid),
                        activity: widget.activity)
                    .then((updated) async {
                  if (updated.runtimeType == Activity) {
                    var activity = updated as Activity;

                    Fluttertoast.showToast(
                      msg: "Commentaire ajouté !",
                      backgroundColor: const Color(0xFF3FC1C9),
                      timeInSecForIosWeb: 3,
                    );
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EventDetail(
                                  category: widget.category,
                                  activity: activity,
                                )));
                  }
                });
              },
              padding: const EdgeInsets.fromLTRB(40, 15, 40, 15),
              style: NeumorphicStyle(
                  shape: NeumorphicShape.flat,
                  boxShape:
                      NeumorphicBoxShape.roundRect(BorderRadius.circular(25)),
                  color: const Color(0xFF3FC1C9),
                  depth: 3,
                  intensity: 3,
                  lightSource: LightSource.topLeft,
                  shadowDarkColor: const Color(0xBFAAAACC),
                  shadowLightColor: const Color(0xFFFFFFFF)),
              child: const Text(
                "Commenter !",
                style: TextStyle(
                    fontSize: 22,
                    color: Color(0xFFEEEEEE),
                    fontWeight: FontWeight.w400),
              )),
        ],
      )
    ]));
  }
}
