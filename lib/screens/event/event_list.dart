import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/model/category.dart' as model;
import 'package:ann_see/screens/category/category.dart';
import 'package:ann_see/screens/event/new_event.dart';
import 'package:ann_see/widgets/event/event_organizer_list_card.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:shimmer/shimmer.dart';

class EventList extends StatefulWidget {
  const EventList({Key? key}) : super(key: key);

  @override
  State<EventList> createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  var organizerRef = FirebaseFirestore.instance
      .collection("organizers")
      .withConverter<Organizer>(
          fromFirestore: (snapshot, _) => Organizer.fromJson(snapshot.data()!),
          toFirestore: (organizer, _) => organizer.toJson())
      .doc(FirebaseAuth.instance.currentUser!.uid);

  late var organizerActivitiesStream = FirebaseFirestore.instance
      .collection("activities")
      .withConverter<Activity>(
          fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
          toFirestore: (activity, _) => activity.toJson())
      .where("organizer", isEqualTo: organizerRef)
      .snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "Vos activités",
                      style: TextStyle(
                          fontSize: 40,
                          color: Color(0xCC222831),
                          fontWeight: FontWeight.w700,
                          fontFamily: "Rubik"),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 50,
                ),
                Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: organizerActivitiesStream,
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> query) {
                      if (query.hasError) {
                        return const Text("Something went wrong");
                      }

                      if (query.hasData &&
                          query.connectionState == ConnectionState.active) {
                        var organizerActivities =
                            query.data!.docs.where((DocumentSnapshot snapshot) {
                          return (snapshot.data() as Activity).organizer ==
                              organizerRef;
                        });

                        if (organizerActivities.isNotEmpty) {
                          return ListView.builder(
                              itemCount: organizerActivities.length,
                              itemBuilder: (BuildContext context, int index) {
                                var activity = organizerActivities
                                    .elementAt(index)
                                    .data() as Activity;

                                return FutureBuilder<
                                        DocumentSnapshot<model.Category>>(
                                    future: activity.category
                                        .withConverter(
                                            fromFirestore: (fireStoreSnapshot,
                                                    _) =>
                                                model.Category.fromJson(
                                                    fireStoreSnapshot.data()!),
                                            toFirestore: (category, _) =>
                                                (category as model.Category)
                                                    .toJson())
                                        .get(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<DocumentSnapshot>
                                            snapshot) {
                                      if (snapshot.hasError) {
                                        return const Text(
                                            "Something went wrong");
                                      }

                                      if (snapshot.hasData &&
                                          snapshot.connectionState ==
                                              ConnectionState.done) {
                                        model.Category category = snapshot.data!
                                            .data() as model.Category;

                                        return Align(
                                            child: EventOrganizerListCard(
                                          activity: activity,
                                          category: category,
                                        ));
                                      }

                                      // return const Align(
                                      //     child: Text("Loading..."));
                                      return Align(
                                          child: Container(
                                              height: 120.0,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.5,
                                              padding: const EdgeInsets.only(
                                                  top: 10.0,
                                                  left: 10,
                                                  right: 10.0,
                                                  bottom: 10.0),
                                              color: Colors.transparent,
                                              child: Shimmer.fromColors(
                                                period: const Duration(
                                                    milliseconds: 500),
                                                baseColor:
                                                    const Color(0xFFEEEEEE),
                                                highlightColor:
                                                    const Color(0xFF3FC1C9),
                                                child: Container(
                                                  decoration:
                                                      const BoxDecoration(
                                                    color: Color(0xFF3FC1C9),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10.0)),
                                                  ),
                                                  child: const Center(
                                                    child: Text(
                                                      "",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 22),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              )));
                                    });
                              });
                        }
                      }

                      return Align(
                          child: Container(
                              height: 120.0,
                              width: MediaQuery.of(context).size.width / 1.5,
                              padding: const EdgeInsets.only(
                                  top: 10.0,
                                  left: 10,
                                  right: 10.0,
                                  bottom: 10.0),
                              color: Colors.transparent,
                              child: Shimmer.fromColors(
                                period: const Duration(milliseconds: 500),
                                baseColor: const Color(0xFFEEEEEE),
                                highlightColor: const Color(0xFF3FC1C9),
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Color(0xFF3FC1C9),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 22),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              )));
                    },
                  ),
                ),
                const Spacer(),
                NeumorphicButton(
                  key: null,
                  onPressed: () {
                    setState(() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const NewEvent(
                                    backArrow: true,
                                  )));
                    });
                  },
                  style: const NeumorphicStyle(
                      shape: NeumorphicShape.concave,
                      boxShape: NeumorphicBoxShape.circle(),
                      color: Color(0xFFEEEEEE),
                      lightSource: LightSource.topLeft,
                      shadowDarkColor: Color(0xFFFFFFFF),
                      shadowLightColor: Color(0xBFAAAACC),
                      intensity: 2,
                      depth: 2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      NeumorphicIcon(
                        Icons.add,
                        size: 75,
                        style: const NeumorphicStyle(
                            color: Colors.green,
                            lightSource: LightSource.topLeft,
                            depth: 2,
                            intensity: 2),
                      ),
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(40, 50, 40, 10),
                  child: ann_see.BottomNavigationBar(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
