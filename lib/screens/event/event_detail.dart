import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart';
import 'package:ann_see/screens/event/event_review.dart';
import 'package:ann_see/screens/event/event_review.dart';
import 'package:ann_see/screens/event/event_write_review.dart';
import 'package:ann_see/widgets/event/detail/event_detail_call_to_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:maps_launcher/maps_launcher.dart';
import '../category/category_detail.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class EventDetail extends StatefulWidget {
  final Category category;
  final Activity activity;

  const EventDetail({
    Key? key,
    required this.category,
    required this.activity,
  }) : super(key: key);

  @override
  State<EventDetail> createState() => _EventDetail();
}

class _EventDetail extends State<EventDetail> {
  @override
  Widget build(BuildContext context) {
    void subscribeUserToEvent() {
      // TODO: Implements user subscription to event
    }

    void createReminderForSpecificEvent() {
      // TODO: Implements create a reminder to the current event, it may be an alert, a notification whatever ...
    }

    void goToEventReview(Category category, Activity activity) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  EventReview(category: category, activity: activity)));
    }

    void goToEventWriteReview(Category category, Activity activity) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  EventWriteReview(category: category, activity: activity)));
    }

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              ShaderMask(
                shaderCallback: (rect) {
                  return const LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.black, Colors.transparent],
                  ).createShader(Rect.fromLTRB(
                      0, rect.height * 0.8, rect.width, rect.height));
                },
                blendMode: BlendMode.dstIn,
                child: Image.network(
                  widget.activity.url,
                  height: MediaQuery.of(context).size.height / 1.8,
                  fit: BoxFit.cover,
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(40),
                child: BackButton(
                  color: Color(0xFFEEEEEE),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 30, 0, 5),
            child: Text(
              widget.activity.label,
              style: const TextStyle(
                  fontSize: 22,
                  color: Color(0xE6222831),
                  fontWeight: FontWeight.w700,
                  fontFamily: "Avenir Next"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 0, 0, 30),
            child: Text(
              widget.category.label,
              style: const TextStyle(
                  fontSize: 14,
                  color: Color(0x80222831),
                  fontWeight: FontWeight.w200,
                  fontFamily: "Avenir Next"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 0, 40, 30),
            child: SizedBox(
              height: 75,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Text(
                  widget.activity.description,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                      fontSize: 16,
                      color: Color(0xCC222831),
                      fontWeight: FontWeight.w200,
                      fontFamily: "Avenir Next"),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            // Adding 3 padding allows me to display neumorphic shadows
            padding: const EdgeInsets.fromLTRB(40, 5, 0, 5),
            child: Row(
              children: [
                EventDetailCallToAction(
                    actionText: "Participer",
                    iconData: Icons.add,
                    onPressed: subscribeUserToEvent,
                    color: const Color(0xFF00ADB5)),
                EventDetailCallToAction(
                    actionText: "J'y vais",
                    iconData: Icons.directions_walk,
                    onPressed: () => openEventOnMaps(widget.activity),
                    color: const Color(0xFF00ADB5)),
                EventDetailCallToAction(
                    actionText: "Rappel",
                    iconData: Icons.alarm_add_rounded,
                    onPressed: createReminderForSpecificEvent,
                    color: const Color(0xFFFF75A0)),
                EventDetailCallToAction(
                    actionText: "Avis",
                    iconData: Icons.star_border_outlined,
                    onPressed: () =>
                        goToEventReview(widget.category, widget.activity),
                    color: const Color(0xFFFCE38A)),
                EventDetailCallToAction(
                    actionText: "Commenter",
                    iconData: Icons.message_outlined,
                    onPressed: () =>
                        goToEventWriteReview(widget.category, widget.activity),
                    color: const Color(0xFFFCE38A))
              ],
            ),
          )
        ],
      ),
    );
  }

  void subscribeUserToEvent() {
    // TODO: Implements user subscription to event
  }

  void openEventOnMaps(Activity activity) {
    MapsLauncher.launchCoordinates(
        activity.localisation.latitude, activity.localisation.longitude);
  }

  void createReminderForSpecificEvent() {
    // TODO: Implements create a reminder to the current event, it may be an alert, a notification whatever ...
  }

  void goToEventOpinion() {
    // TODO: Implements the navigation to go to the opinion page
  }
}
