import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/screens/game/cultural_profile_game.dart';
import 'package:ann_see/screens/profile/organizer_profile.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/viewmodel/shared_viewmodel.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_ml_model_downloader/firebase_ml_model_downloader.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

FirebaseAuth auth = FirebaseAuth.instance;

class _LoginState extends State<Login> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool spinnerVisible = false;

  @override
  Widget build(BuildContext context) {
    Future<bool> signIn() {
      return Provider.of<AuthViewModel>(context, listen: false).signIn(
          email: emailController.text.toString().trim(),
          password: passwordController.text.toString().trim());
    }

    void downloadModel(
        {required String modelName,
        required Future<dynamic> Function() completion}) async {
      final FirebaseModelDownloader firebaseModelManager =
          FirebaseModelDownloader.instance;

      await firebaseModelManager
          .getModel(
              modelName,
              FirebaseModelDownloadType.localModelUpdateInBackground,
              FirebaseModelDownloadConditions(
                  iosAllowsBackgroundDownloading: true,
                  iosAllowsCellularAccess: true))
          .then((value) {
            Provider.of<SharedViewModel>(context, listen: false).model =
                value.file;
          })
          .whenComplete(completion)
          .catchError((error, stackTrace) {
            Fluttertoast.showToast(
                msg:
                    "Téléchargement échoué, recommandations indisponibles. Vérifiez votre connexion. ❌",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          });
    }

    // void authenticateAsAnonymousUserAndSendLoginEvent() async {
    //   await FirebaseAuth.instance.signInAnonymously();
    //   await FirebaseAnalytics.instance.logLogin();
    // }

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset("assets/logo/logo.svg",
                color: const Color(0xFF3FC1C9), height: 175, width: 175),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(top: 60),
                      child: Text(
                        "Bienvenue",
                        style: TextStyle(
                            fontSize: 28,
                            color: Color(0xFF222831),
                            fontWeight: FontWeight.w800,
                            letterSpacing: 1,
                            fontFamily: "Rubik"),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 60),
                      child: Text("Commençons ! 😁",
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Color(0xCC222831),
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.5,
                              fontFamily: "Avenir Next")),
                    ),
                  ],
                )
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      TextInputField(
                          controller: emailController,
                          hintText: "Email",
                          obscureText: false),
                      TextInputField(
                          controller: passwordController,
                          hintText: "Mot de passe",
                          obscureText: true),
                    ],
                  ),
                ),
              ],
            ),
            // I know this row is shit, but it is necessary to stick the forgot
            // password link at the end of the fields
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                Text(
                  "Mot de passe oublié ?",
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Color(0x99222831),
                      decoration: TextDecoration.underline,
                      fontWeight: FontWeight.w200,
                      fontFamily: "Avenir Next"),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 60),
                  child: spinnerVisible
                      ? const SpinKitFadingCircle(
                          color: Colors.blueGrey,
                          size: 50.0,
                        )
                      : NeumorphicButton(
                          key: null,
                          onPressed: () {
                            if (emailController.text.toString().trim() != "" &&
                                passwordController.text.toString().trim() !=
                                    "") {
                              setState(() {
                                spinnerVisible = true;
                              });
                              signIn().then(((logged) => {
                                    if (logged)
                                      {
                                        downloadModel(
                                            modelName:
                                                Provider.of<SharedViewModel>(
                                                        context,
                                                        listen: false)
                                                    .remoteModelName,
                                            completion: () => Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => Provider
                                                                    .of<AuthViewModel>(
                                                                        context,
                                                                        listen:
                                                                            false)
                                                                .getCurrentUser()
                                                            is Organizer
                                                        ? const OrganizerProfile()
                                                        : const CulturalProfileGame())))
                                      }
                                    else
                                      {
                                        setState(() {
                                          spinnerVisible = false;
                                        }),
                                        Fluttertoast.showToast(
                                            msg:
                                                "Connexion échouée. Veuillez vérifier vos informations de connexion",
                                            toastLength: Toast.LENGTH_LONG,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 3,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0)
                                      }
                                  }));
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Veuillez remplir tous les champs avant de pouvoir vous connecter",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                          style: NeumorphicStyle(
                              shape: NeumorphicShape.flat,
                              boxShape: NeumorphicBoxShape.roundRect(
                                  BorderRadius.circular(25)),
                              color: const Color(0xFFEEEEEE),
                              lightSource: LightSource.bottomRight,
                              shadowDarkColor: const Color(0xBFAAAACC),
                              shadowLightColor: const Color(0xFFFFFFFF),
                              intensity: 5,
                              depth: 5),
                          child: const Text(
                            "Se connecter",
                            style: TextStyle(
                                fontSize: 16,
                                color: Color(0xFF222831),
                                fontWeight: FontWeight.w800,
                                letterSpacing: 0.6,
                                fontFamily: "Avenir Next"),
                          )),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
