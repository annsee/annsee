import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart' as model;
import 'package:ann_see/widgets/category/category_detail_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:shimmer/shimmer.dart';
import '../event/event_detail.dart';

class CategoryDetail extends StatelessWidget {
  final model.Category category;
  final List<Activity>? likedActivities;

  const CategoryDetail({Key? key, required this.category, this.likedActivities})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<dynamic> getMany(dynamic documentRefs) {
      return documentRefs
          .map((docRef) => (docRef as DocumentReference).snapshots())
          .toList();
    }

    void cardPressed(Activity activity) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  EventDetail(category: category, activity: activity)));
    }

    return Scaffold(
        body: Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xFF3FC1C9),
              Color(0xFFEEEEEE),
            ],
          )),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 80, 0, 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 40),
                  child: Row(
                    children: [
                      const BackButton(color: Color(0xFFEEEEEE)),
                      Flexible(
                        child: Text(
                          category.label.toUpperCase(),
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              fontSize: 32,
                              fontWeight: FontWeight.w800,
                              letterSpacing: 1,
                              color: Color(0xFFEEEEEE)),
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                    child: ListView(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        children: likedActivities == null
                            ? getMany(category.activities).map((snapshot) {
                                return StreamBuilder<DocumentSnapshot>(
                                  stream: snapshot as Stream<
                                      DocumentSnapshot<Map<String, dynamic>>>,
                                  builder: (BuildContext context,
                                      AsyncSnapshot<DocumentSnapshot>
                                          snapshot) {
                                    if (snapshot.hasError) {
                                      return const Text("Something went wrong");
                                    }

                                    if (snapshot.hasData &&
                                        !snapshot.data!.exists) {
                                      return const Text(
                                          "Document does not exist");
                                    }

                                    if (snapshot.connectionState ==
                                        ConnectionState.active) {
                                      Activity activity = Activity.fromJson(
                                          snapshot.data!.data()
                                              as Map<String, dynamic>);
                                      return CategoryDetailCard(
                                        title: activity.label,
                                        description: activity.description,
                                        onPressed: () => cardPressed(activity),
                                      );
                                    }

                                    return Container(
                                        height: 120.0,
                                        width: 75.0,
                                        padding: const EdgeInsets.only(
                                            top: 10.0,
                                            left: 20,
                                            right: 20.0,
                                            bottom: 15.0),
                                        margin: const EdgeInsets.only(
                                          left: 20.0,
                                          right: 20.0,
                                        ),
                                        color: Colors.transparent,
                                        child: Shimmer.fromColors(
                                          period:
                                              const Duration(milliseconds: 500),
                                          baseColor: const Color(0xFFEEEEEE),
                                          highlightColor:
                                              const Color(0xFF3FC1C9),
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0xFF3FC1C9),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                            ),
                                            child: const Center(
                                              child: Text(
                                                "",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 22),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        )); // Loading list Activity from category
                                  },
                                );
                              }).toList()
                            : likedActivities!.map((activity) {
                                return StreamBuilder<QuerySnapshot<Activity>>(
                                  stream: FirebaseFirestore.instance
                                      .collection("activities")
                                      .where("label", isEqualTo: activity.label)
                                      .withConverter(
                                          fromFirestore: (snapshot, _) =>
                                              Activity.fromJson(
                                                  snapshot.data()!),
                                          toFirestore: (Activity activity, _) =>
                                              activity.toJson())
                                      .snapshots(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<QuerySnapshot<Activity>>
                                          snapshot) {
                                    if (snapshot.hasError) {
                                      return const Text("Something went wrong");
                                    }

                                    if (snapshot.hasData &&
                                        !snapshot.data!.docs.first.exists) {
                                      return const Text(
                                          "Document does not exist");
                                    }

                                    if (snapshot.connectionState ==
                                        ConnectionState.active) {
                                      Activity activity =
                                          snapshot.data!.docs.first.data();
                                      return CategoryDetailCard(
                                        title: activity.label,
                                        description: activity.description,
                                        onPressed: () => cardPressed(activity),
                                      );
                                    }

                                    return Container(
                                        height: 120.0,
                                        width: 75.0,
                                        padding: const EdgeInsets.only(
                                            top: 10.0,
                                            left: 20,
                                            right: 20.0,
                                            bottom: 15.0),
                                        margin: const EdgeInsets.only(
                                          left: 20.0,
                                          right: 20.0,
                                        ),
                                        color: Colors.transparent,
                                        child: Shimmer.fromColors(
                                          period:
                                              const Duration(milliseconds: 500),
                                          baseColor: const Color(0xFFEEEEEE),
                                          highlightColor:
                                              const Color(0xFF3FC1C9),
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              color: Color(0xFF3FC1C9),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                            ),
                                            child: const Center(
                                              child: Text(
                                                "",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 22),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        )); // Loading list Activity from top category;
                                  },
                                );
                              }).toList())),
              ],
            ),
          ),
        )
      ],
    ));
  }
}
