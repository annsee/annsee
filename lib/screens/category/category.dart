import 'package:ann_see/screens/category/category_detail.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/shared_viewmodel.dart';
import 'package:ann_see/widgets/category/category_card.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ann_see/model/category.dart' as model;
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class Category extends StatefulWidget {
  const Category({Key? key}) : super(key: key);

  @override
  State<Category> createState() => _Category();
}

class _Category extends State<Category> {
  int buttonSelected = 3;

  final Stream<QuerySnapshot> _categoriesStream = FirebaseFirestore.instance
      .collection('categories')
      .withConverter<model.Category>(
          fromFirestore: (snapshot, _) =>
              model.Category.fromJson(snapshot.data()!),
          toFirestore: (category, _) => category.toJson())
      .snapshots();

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(40, 15, 40, 0),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                child: Text(
                  "Parcourir les catégories",
                  style: TextStyle(
                      fontSize: 28,
                      color: Color(0xFF222831),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Rubik"),
                ),
              ),
              Flexible(
                  child: StreamBuilder<QuerySnapshot>(
                      stream: _categoriesStream,
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.hasError) {
                          return const Text('Something went wrong');
                        }

                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          Container contSkel = Container(
                              height: 100.0,
                              width: 100.0,
                              padding: const EdgeInsets.only(
                                  top: 10.0,
                                  left: 10,
                                  right: 10.0,
                                  bottom: 15.0),
                              color: Colors.transparent,
                              child: Shimmer.fromColors(
                                period: const Duration(milliseconds: 500),
                                baseColor: const Color(0xFFEEEEEE),
                                highlightColor: const Color(0xFF3FC1C9),
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Color(0xFF3FC1C9),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 22),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ));
                          return GridView.count(crossAxisCount: 2, children: [
                            contSkel,
                            contSkel,
                            contSkel,
                            contSkel,
                            contSkel,
                            contSkel
                          ]); //loading categories cards
                        }

                        if (Provider.of<CategoryViewModel>(context,
                                    listen: true)
                                .getCategories() ==
                            null) {
                          Provider.of<CategoryViewModel>(context, listen: true)
                              .setCategories(snapshot.data!.docs
                                  .map((DocumentSnapshot document) {
                            model.Category category =
                                document.data()! as model.Category;

                            return category;
                          }).toList());
                        }

                        return Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                            child: GridView.count(
                              crossAxisCount: 2,
                              shrinkWrap: true,
                              children: snapshot.data!.docs.map((
                                DocumentSnapshot document,
                              ) {
                                model.Category category =
                                    document.data()! as model.Category;

                                return CategoryCard(
                                  iconData: Provider.of<SharedViewModel>(
                                          context,
                                          listen: false)
                                      .icons[category.icon],
                                  onPressed: () => buttonPressed(category),
                                  title: category.label,
                                  color: Provider.of<SharedViewModel>(context,
                                          listen: false)
                                      .colors[category.color]!,
                                );
                              }).toList(),
                            ));
                      })),
              const Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: ann_see.BottomNavigationBar(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void buttonPressed(model.Category category) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CategoryDetail(
                  category: category,
                )));
  }
}
