import 'package:ann_see/screens/game/cultural_profile_game.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/widgets/registration/registration_button.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class UserRegistration extends StatefulWidget {
  const UserRegistration({Key? key}) : super(key: key);

  @override
  State<UserRegistration> createState() => _UserRegistrationState();
}

enum Gender {
  homme("Homme"),
  femme("Femme");

  const Gender(this.value);
  final String value;
}

class _UserRegistrationState extends State<UserRegistration> {
  Gender? _gender = Gender.homme;

  final nomController = TextEditingController();
  final prenomController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool spinnerVisible = false;

  @override
  Widget build(BuildContext context) {
    bool isFormValid() {
      return nomController.text.toString().trim() != "" &&
          prenomController.text.toString().trim() != "" &&
          emailController.text.toString().trim() != "" &&
          passwordController.text.toString().trim() != "";
    }

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  BackButton(),
                  Text(
                    "Particulier",
                    style: TextStyle(
                        fontSize: 28,
                        color: Color(0xCC222831),
                        fontWeight: FontWeight.w700,
                        fontFamily: "Rubik"),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                TextInputField(
                    controller: nomController,
                    hintText: "Nom",
                    obscureText: false),
                TextInputField(
                    controller: prenomController,
                    hintText: "Prénom",
                    obscureText: false),
                TextInputField(
                    controller: emailController,
                    hintText: "Email",
                    obscureText: false),
                TextInputField(
                    controller: passwordController,
                    hintText: "Mot de passe",
                    obscureText: true)
              ],
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 40),
              child: Text(
                "Vous êtes ...",
                style: TextStyle(
                    fontSize: 18.0,
                    color: Color(0xFF222831),
                    fontWeight: FontWeight.normal,
                    fontFamily: "Avenir Next"),
              ),
            ),
            ListTile(
              title: const Text('Un homme'),
              leading: Radio<Gender>(
                value: Gender.homme,
                groupValue: _gender,
                onChanged: (Gender? value) {
                  setState(() {
                    _gender = value;
                  });
                },
              ),
            ),
            ListTile(
              title: const Text('Une femme'),
              leading: Radio<Gender>(
                value: Gender.femme,
                groupValue: _gender,
                onChanged: (Gender? value) {
                  setState(() {
                    _gender = value;
                  });
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: spinnerVisible
                      ? const SpinKitFadingCircle(
                          color: Colors.blueGrey,
                          size: 70.0,
                        )
                      : RegistrationButton(onPressed: () {
                          if (isFormValid()) {
                            if (passwordController.text.length >= 6) {
                              setState(() {
                                spinnerVisible = true;
                              });
                              registerNewUser().then((created) => {
                                    if (created)
                                      {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const CulturalProfileGame()))
                                      }
                                    else
                                      {
                                        setState(() {
                                          spinnerVisible = false;
                                        }),
                                        Fluttertoast.showToast(
                                            msg:
                                                "Inscription échouée. Veuillez contacter un administrateur de la plateforme.",
                                            toastLength: Toast.LENGTH_LONG,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 3,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0)
                                      }
                                  });
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Le mot de passe doit contenir 6 caractères !",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          } else {
                            Fluttertoast.showToast(
                                msg:
                                    "Veuillez remplir tous les champs avabt de pouvoir vous inscrire",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 3,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        }),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> registerNewUser() {
    return Provider.of<AuthViewModel>(context, listen: false).userRegister(
        email: emailController.text.toString().trim(),
        password: passwordController.text.toString().trim(),
        nom: nomController.text.toString().trim(),
        prenom: prenomController.text.toString().trim(),
        genre: _gender!.value.toString());
  }
}
