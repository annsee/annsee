import 'package:ann_see/screens/game/cultural_profile_game.dart';
import 'package:ann_see/screens/profile/organizer_profile.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/widgets/registration/registration_button.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:provider/provider.dart';

class OrganizerRegistration extends StatefulWidget {
  const OrganizerRegistration({Key? key}) : super(key: key);

  @override
  State<OrganizerRegistration> createState() => _OrganizerRegistrationState();
}

class _OrganizerRegistrationState extends State<OrganizerRegistration> {
  String? _dropdownValue = "Commerce";

  final organizationNameController = TextEditingController();
  final organizationEmailController = TextEditingController();
  final organizationPhoneController = TextEditingController();
  final organizationPasswordController = TextEditingController();

  bool spinnerVisible = false;

  @override
  Widget build(BuildContext context) {
    bool isFormValid() {
      return organizationNameController.text.toString().trim() != "" &&
          organizationEmailController.text.toString().trim() != "" &&
          organizationPhoneController.text.toString().trim() != "" &&
          organizationPasswordController.text.toString().trim() != "";
    }

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  BackButton(),
                  Text(
                    "Organisateur",
                    style: TextStyle(
                        fontSize: 28,
                        color: Color(0xCC222831),
                        fontWeight: FontWeight.w700,
                        fontFamily: "Rubik"),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Column(
                children: [
                  TextInputField(
                      controller: organizationNameController,
                      hintText: "Nom de l'organisateur",
                      obscureText: false),
                  TextInputField(
                      controller: organizationEmailController,
                      hintText: "Email de l'organisateur",
                      obscureText: false),
                  TextInputField(
                      controller: organizationPhoneController,
                      hintText: "N° de téléphone de l'organisateur",
                      obscureText: false),
                  TextInputField(
                      controller: organizationPasswordController,
                      hintText: "Mot de passe",
                      obscureText: true)
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Text(
                "Quel type d'organisateur êtes-vous ?",
                style: TextStyle(
                    fontSize: 18.0,
                    color: Color(0xFF222831),
                    fontWeight: FontWeight.normal,
                    fontFamily: "Avenir Next"),
              ),
            ),
            DropdownButton(
              items: const [
                DropdownMenuItem(child: Text("Commerce"), value: "Commerce"),
                DropdownMenuItem(child: Text("Outdoor"), value: "Outdoor"),
                DropdownMenuItem(
                    child: Text("Restaurant"), value: "Restaurant"),
                DropdownMenuItem(child: Text("Bar"), value: "Bar"),
                DropdownMenuItem(
                    child: Text("Patrimoine"), value: "Patrimoine"),
                DropdownMenuItem(child: Text("Rencontre"), value: "Rencontre"),
                DropdownMenuItem(
                    child: Text("Activité nocturne"),
                    value: "Activité nocturne"),
              ],
              value: _dropdownValue,
              onChanged: dropDownCallback,
              icon: const Icon(Icons.arrow_drop_down),
              elevation: 16,
              underline: Container(
                height: 2,
                color: const Color(0xFF00ADB5),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: spinnerVisible
                      ? const SpinKitFadingCircle(
                          color: Colors.blueGrey,
                          size: 70.0,
                        )
                      : RegistrationButton(onPressed: () {
                          if (isFormValid()) {
                            if (organizationPasswordController.text.length >=
                                6) {
                              setState(() {
                                spinnerVisible = true;
                              });
                              registerNewOrganization().then((created) => {
                                    if (created)
                                      {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const OrganizerProfile()))
                                      }
                                    else
                                      {
                                        setState(() {
                                          spinnerVisible = false;
                                        }),
                                        Fluttertoast.showToast(
                                            msg:
                                                "Inscription échouée. Veuillez contacter un administrateur de la plateforme.",
                                            toastLength: Toast.LENGTH_LONG,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 3,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0)
                                      }
                                  });
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Le mot de passe doit contenir 6 caractères !",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 3,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          } else {
                            Fluttertoast.showToast(
                                msg:
                                    "Veuillez remplir tous les champs avabt de pouvoir vous inscrire",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 3,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        }),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> registerNewOrganization() {
    return Provider.of<AuthViewModel>(context, listen: false).organizerRegister(
        email: organizationEmailController.text.toString().trim(),
        password: organizationPasswordController.text.toString().trim(),
        nom: organizationNameController.text.toString().trim(),
        phone: organizationPhoneController.text.toString().trim(),
        category: _dropdownValue!);
  }

  void dropDownCallback(String? selectedValue) {
    if (selectedValue is String) {
      setState(() {
        _dropdownValue = selectedValue;
      });
    }
  }
}
