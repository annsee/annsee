import 'package:ann_see/screens/register/organizer_registration.dart';
import 'package:ann_see/screens/register/user_registration.dart';
import 'package:ann_see/widgets/registration/registration_choice_button.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class RegistrationChoice extends StatefulWidget {
  const RegistrationChoice({Key? key}) : super(key: key);

  @override
  State<RegistrationChoice> createState() => _RegistrationChoiceState();
}

class _RegistrationChoiceState extends State<RegistrationChoice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.all(40),
          decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
          child: SafeArea(
              child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 60),
                child: Row(
                  children: const [
                    BackButton(),
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        "Êtes-vous ...",
                        style: TextStyle(
                          fontSize: 22,
                          color: Color(0xE6222831),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Rubik",
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                    child: RegistrationChoiceButton(
                      color: const Color(0xFF3FC1C9),
                      iconData: Icons.business_rounded,
                      onPressed: goToOrganizerRegistration,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 60),
                    child: Text(
                      "Un organisateur".toUpperCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18,
                          color: Color(0xFF3FC1C9),
                          fontWeight: FontWeight.w800,
                          fontFamily: "Avenir Next",
                          letterSpacing: 1),
                    ),
                  ),
                  const Divider(
                    height: 1.5,
                    thickness: 1,
                    indent: 20,
                    endIndent: 20,
                    color: Color(0x55222831),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 60, 0, 20),
                    child: RegistrationChoiceButton(
                      color: const Color(0xD9FF75A0),
                      iconData: Icons.person_outline_rounded,
                      onPressed: goToUserRegistration,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 40),
                    child: Text(
                      "Un particulier".toUpperCase(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18,
                          color: Color(0xD9FF75A0),
                          fontWeight: FontWeight.w800,
                          fontFamily: "Avenir Next",
                          letterSpacing: 1),
                    ),
                  )
                ],
              ),
            ],
          ))),
    );
  }

  goToUserRegistration() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const UserRegistration()),
      );
    });
  }

  goToOrganizerRegistration() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const OrganizerRegistration()),
      );
    });
  }
}
