import 'dart:collection';
import 'dart:ffi';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart';
import 'package:ann_see/screens/event/event_detail.dart';
import 'package:ann_see/viewmodel/activity_viewmodel.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/game_viewmodel.dart';
import 'package:ann_see/viewmodel/shared_viewmodel.dart';
import 'package:ann_see/widgets/recommended_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:provider/provider.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:shimmer/shimmer.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

class Recommendation extends StatefulWidget {
  const Recommendation({Key? key}) : super(key: key);

  @override
  State<Recommendation> createState() => _RecommendationState();
}

class _RecommendationState extends State<Recommendation> {
  List<Activity> likedActivities = List.empty(growable: true);
  Interpreter? tflite;

  void initializeInterpreter({required File modelFile}) {
    final Delegate gpuDelegate;

    // Platform.isAndroid
    //     ? {
    //         gpuDelegate = GpuDelegateV2(
    //             options: GpuDelegateOptionsV2(
    //           isPrecisionLossAllowed: false,
    //           inferencePreference: TfLiteGpuInferenceUsage.fastSingleAnswer,
    //           inferencePriority1: TfLiteGpuInferencePriority.minLatency,
    //           inferencePriority2: TfLiteGpuInferencePriority.auto,
    //           inferencePriority3: TfLiteGpuInferencePriority.auto,
    //         ))
    //       }
    //     : {
    //         gpuDelegate = GpuDelegate(
    //             options: GpuDelegateOptions(
    //                 allowPrecisionLoss: true,
    //                 waitType: TFLGpuDelegateWaitType.active))
    //       };

    // var interpreterOptions = InterpreterOptions()..addDelegate(gpuDelegate);
    var interpreterOptions = InterpreterOptions()..threads = 4;
    tflite = Interpreter.fromFile(modelFile, options: interpreterOptions);
  }

  dynamic loadLocalModel() async {
    initializeInterpreter(
        modelFile: Provider.of<SharedViewModel>(context, listen: false).model);
  }

  void loadLikedActivities() {
    Provider.of<GameViewModel>(context, listen: false)
        .likes
        .forEach((categoryKey, listActivities) {
      likedActivities.addAll(listActivities);
    });
  }

  void load() {
    // loadLocalModel();
    loadLikedActivities();
  }

  LinkedHashMap<String, int> preProcess(
      {required List<Activity> likedActivities}) {
    // List<int> inputContext = List<int>.filled(
    //     Provider.of<SharedViewModel>(context, listen: false).defaultInputLength,
    //     0);
    // for (var i = 0;
    //     i <
    //         Provider.of<SharedViewModel>(context, listen: false)
    //             .defaultInputLength;
    //     i++) {
    //   if (i < likedActivities.length) {
    //     inputContext[i] = likedActivities[i].id;
    //   } else {
    //     inputContext[i] =
    //         Provider.of<SharedViewModel>(context, listen: false).padID;
    //   }
    // }
    // return inputContext;

    var numberOfLikes = Provider.of<GameViewModel>(context, listen: false)
        .likes
        .map(((String category, List<Activity?> activities) {
      return MapEntry(category, activities.length);
    }));

    int numberOfLikedCategories = 0;

    numberOfLikes.forEach((key, value) {
      if (value > 0) {
        numberOfLikedCategories += 1;
      }
    });

    final sortedLikesKeys = numberOfLikes.keys.toList(growable: false)
      ..sort((String k1, String k2) =>
          numberOfLikes[k2]!.compareTo(numberOfLikes[k1]!));
    LinkedHashMap<String, int> sortedLikes = LinkedHashMap.fromIterable(
        sortedLikesKeys.take(numberOfLikedCategories),
        key: (k) => k,
        value: (k) => numberOfLikes[k]!);

    return sortedLikes;
  }

  List<Result> recommend({required List<Activity> likedActivities}) {
    var preProcessedList = preProcess(likedActivities: likedActivities);

    // List<Object> inputs = List<Object>.generate(
    //     preProcessedList.length, (index) => [preProcessedList[index]]);

    // List<int> outputIds = List<int>.filled(
    //     Provider.of<SharedViewModel>(context, listen: false)
    //         .defaultOutputLength,
    //     0);
    // List<double> confidences = List<double>.filled(
    //     Provider.of<SharedViewModel>(context, listen: false)
    //         .defaultOutputLength,
    //     0.0);
    // Map<int, Object> outputs = HashMap();
    // outputs[Provider.of<SharedViewModel>(context, listen: false)
    //     .defaultOutputIDsIndex] = outputIds;
    // outputs[Provider.of<SharedViewModel>(context, listen: false)
    //     .defaultOutputScoresIndex] = confidences;

    // For ex: if input tensor shape [1,5] and type is float32
    // var input = [
    //   [7, 11, 0, 0, 0, 0, 0, 0, 0, 0]
    // ];

// if output tensor shape [1,2] and type is float32
    // var output = List.filled(2, []);

// inference
    // tflite?.runForMultipleInputs(inputs, outputs);
    // print(outputs);

    List<Activity> sortedActivities = [];
    // List<double> confidences = [];

    Map<String, List<Activity>> activitiesByCategory = {
      "0I12MJ8woN5xZhAvKvsn": [],
      "3ImKjX3ILvusz2MEYDkX": [],
      "9Z4d5zQBHSZFtrQI7PQM": [],
      "IQ0f6L3UnuCsGRnFaiwj": [],
      "SsuyCLIEF69aXXz9W3Ag": [],
      "TkOHgppbsef3zaHJSAqW": [],
      "V450a8jPUDiIw1vqYDjQ": [],
      "cR8jE7AuNH9iU8avvvOA": [],
      "hhj6wvsSm3CL23AAtgDQ": [],
      "qQj5dPpogthMUMcOKSPo": [],
    };

    Provider.of<ActivityViewModel>(context, listen: false)
        .getActivities()!
        .forEach((activity) {
      activitiesByCategory.forEach((key, value) {
        if (key == activity.category.id) {
          value.add(activity);
        }
      });
    });

    preProcessedList.forEach((key, value) {
      var activities = activitiesByCategory[key];

      for (var i = 0;
          i < (activities!.length >= 3 ? 3 : activities.length);
          i++) {
        if (!Provider.of<GameViewModel>(context, listen: false)
            .likes[key]!
            .contains(activities[i])) {
          sortedActivities.add(activities[i]);
        }
        // confidences.add(Random().nextDouble() * (value / 5) + 1);
      }
    });

    return postProcess(
        // outputIds: outputIds,
        // confidences: confidences,
        likedActivities: sortedActivities);
  }

  List<Result> postProcess(
      {
      // required List<int> outputIds,
      // required List<double> confidences,
      required List<Activity> likedActivities}) {
    List<Result> results = List.empty(growable: true);
    // for (var i in outputIds) {
    //   if (results.length >=
    //       Provider.of<SharedViewModel>(context, listen: false).defaultTopK) {
    //     print(
    //         "Selected top K: ${Provider.of<SharedViewModel>(context, listen: false).defaultTopK}. Ignore the rest.");
    //     break;
    //   }
    //   int id = outputIds[i];
    //   Activity item = likedActivities[i];
    //   if (item == null) {
    //     print("Inference output[$i]. Id: $id is null");
    //     continue;
    //   }
    //   if (likedActivities.contains(item)) {
    //     print("Inference output[$i]. Id: $id is contained");
    //     continue;
    //   }
    //   Result result = Result(id: id, item: item, confidence: confidences[i]);
    //   results.add(result);
    //   print("Inference output[$i]. Result: $result");
    // }

    for (var activity in likedActivities) {
      if (activity.id != null) {
        Result result =
            Result(id: activity.id!, item: activity, confidence: 0.00);
        results.add(result);
      }
    }

    return results;
  }

  @override
  Widget build(BuildContext context) {
    load();
    // preProcess(likedActivities: likedActivities);
    List<Result> results = recommend(likedActivities: likedActivities);

    return Scaffold(
        body: Container(
            padding: const EdgeInsets.fromLTRB(40, 15, 40, 0),
            child: SafeArea(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: const Text(
                      "Recommandations",
                      style: TextStyle(
                          fontSize: 36,
                          color: Color(0xCC222831),
                          fontWeight: FontWeight.w700,
                          fontFamily: "Rubik"),
                    ),
                  ),
                  Flexible(
                    child: results.isNotEmpty
                        ? ListView.builder(
                            itemBuilder: (context, index) {
                              return FutureBuilder<DocumentSnapshot>(
                                future: Provider.of<CategoryViewModel>(context,
                                        listen: false)
                                    .get(uid: results[index].item.category.id),
                                builder: (BuildContext context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.hasError) {
                                    return const Text("Something went wrong");
                                  }

                                  if (snapshot.hasData &&
                                      !snapshot.data!.exists) {
                                    return const Text(
                                        "Document does not exist");
                                  }

                                  if (snapshot.connectionState ==
                                      ConnectionState.done) {
                                    Category category =
                                        snapshot.data!.data() as Category;

                                    return RecommendedCard(
                                        image: results[index].item.url,
                                        label: results[index].item.label,
                                        onPressed: () => {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          EventDetail(
                                                            category: category,
                                                            activity:
                                                                results[index]
                                                                    .item,
                                                          )))
                                            });
                                  }

                                  return Container(
                                      height: 120.0,
                                      width: 75.0,
                                      padding: const EdgeInsets.only(
                                          top: 10.0,
                                          left: 20,
                                          right: 20.0,
                                          bottom: 15.0),
                                      margin: const EdgeInsets.only(
                                        left: 20.0,
                                        right: 20.0,
                                      ),
                                      color: Colors.transparent,
                                      child: Shimmer.fromColors(
                                        period:
                                            const Duration(milliseconds: 500),
                                        baseColor: const Color(0xFFEEEEEE),
                                        highlightColor: const Color(0xFF3FC1C9),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Color(0xFF3FC1C9),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              "",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 22),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      )); // Loading list Activity from recommendation
                                },
                              );

                              // return RecommendedCard(
                              //     image: results[index].item.url,
                              //     label: results[index].item.label,
                              //     onPressed: () => {
                              //           Navigator.push(
                              //               context,
                              //               MaterialPageRoute(
                              //                   builder: (context) =>
                              //                       EventDetail(
                              //                         category: Provider.of<
                              //                                     CategoryViewModel>(
                              //                                 context,
                              //                                 listen: false)
                              //                             .get(
                              //                                 uid:
                              //                                     results[index]
                              //                                         .item
                              //                                         .category
                              //                                         .id),
                              //                         activity:
                              //                             results[index].item,
                              //                       )))
                              //         });
                            },
                            itemCount: results.length,
                          )
                        : ListView.builder(
                            itemBuilder: (context, index) {
                              return const Text(
                                  "Vous n'avez liké aucune activité !");
                            },
                            itemCount: 1,
                          ),
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                    child: ann_see.BottomNavigationBar(),
                  ),
                ]))));
  }
}

class Result {
  Result({required this.id, required this.item, required this.confidence});

  final int id;
  final Activity item;
  final double confidence;

  @override
  String toString() {
    return "[$id] confidence: $confidence, item: ${item.label}";
  }
}
