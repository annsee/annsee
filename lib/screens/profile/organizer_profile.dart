import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/screens/event/new_event.dart';
import 'package:ann_see/screens/register/registration_choice.dart';
import 'package:ann_see/screens/tutorial/tutorial_terms_conditions.dart';
import 'package:ann_see/viewmodel/account_viewmodel.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:ann_see/widgets/profile/profile_button.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:ann_see/widgets/utils/blurry_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class OrganizerProfile extends StatefulWidget {
  const OrganizerProfile({Key? key}) : super(key: key);

  @override
  State<OrganizerProfile> createState() => _OrganizerProfileState();
}

class _OrganizerProfileState extends State<OrganizerProfile> {
  final organizationNameController = TextEditingController();
  final organizationEmailController = TextEditingController();
  final organizationPhoneController = TextEditingController();
  final organizationTypeController = TextEditingController();
  final organizationPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Organizer currentUser =
        Provider.of<AuthViewModel>(context, listen: false).getCurrentUser()
            as Organizer;

    organizationNameController.text = currentUser.nom;
    organizationEmailController.text = currentUser.mail;
    organizationPhoneController.text = currentUser.phone;
    organizationTypeController.text = currentUser.category;

    bool isFormUntouched() {
      return organizationNameController.text.toString().trim() ==
              currentUser.nom &&
          organizationEmailController.text.toString().trim() ==
              currentUser.mail &&
          organizationPhoneController.text.toString().trim() ==
              currentUser.phone &&
          organizationTypeController.text.toString().trim() ==
              currentUser.category &&
          organizationPasswordController.text.toString().trim() == "";
    }

    void saveUserProfileChanges() {
      if (isFormUntouched()) {
        Fluttertoast.showToast(
            msg: "Vous n'avez modifié aucun champ",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else if (organizationPasswordController.text.toString().trim() != "" &&
          organizationPasswordController.text.length < 6) {
        Fluttertoast.showToast(
            msg: "Le mot de passe doit faire 6 caractères ou plus !",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Provider.of<AccountViewModel>(context, listen: false)
            .updateOrganizerAccount(
                name: organizationNameController.text.toString().trim(),
                email: organizationEmailController.text.toString().trim(),
                phone: organizationPhoneController.text.toString().trim(),
                category: organizationTypeController.text.toString().trim(),
                password: organizationPasswordController.text.toString().trim())
            .then((updated) {
          if (updated) {
            setState(() {
              Fluttertoast.showToast(
                msg: "Changements sauvegardés !",
                backgroundColor: const Color(0xFF3FC1C9),
                timeInSecForIosWeb: 3,
              );
            });
          } else {
            Fluttertoast.showToast(
                msg: "Une erreur est survenue (relancez l'application)",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 3,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        });
      }
    }

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: Neumorphic(
                    style: const NeumorphicStyle(
                        border: NeumorphicBorder(
                            color: Color(0xFFEEEEEE), width: 3),
                        boxShape: NeumorphicBoxShape.circle(),
                        lightSource: LightSource.topLeft,
                        shadowLightColorEmboss: Color(0xFFFFFFFF),
                        shadowDarkColorEmboss: Color(0xBFAAAACC),
                        intensity: 3,
                        depth: 3),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/images/user_placeholder.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                ),
                TextInputField(
                    controller: organizationNameController,
                    hintText: "Aboutgoods",
                    obscureText: false),
                TextInputField(
                    controller: organizationEmailController,
                    hintText: "aboutgoods@gmail.com",
                    obscureText: false),
                TextInputField(
                    controller: organizationPhoneController,
                    hintText: "04 70 99 99 99",
                    obscureText: false),
                TextInputField(
                    controller: organizationTypeController,
                    hintText: "Commerce",
                    obscureText: false),
                TextInputField(
                    controller: organizationPasswordController,
                    hintText: "Mot de passe",
                    obscureText: true),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ProfileButton(
                          iconData: Icons.delete_forever_rounded,
                          color: Colors.red,
                          onPressed: deleteUserAccount),
                      ProfileButton(
                          iconData: Icons.save_rounded,
                          color: Colors.green,
                          onPressed: saveUserProfileChanges),
                    ],
                  ),
                ),
                const ann_see.BottomNavigationBar()
              ],
            ),
            Positioned(
              top: 15,
              right: 0,
              child: ProfileButton(
                  iconData: Icons.logout,
                  color: const Color(0xFF3FC1C9),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const TutorialTermsAndConditions()));
                  }),
            ),
          ],
        ),
      ),
    );
  }

  void deleteUserAccount() {
    VoidCallback continueCallBack = () {
      Provider.of<AccountViewModel>(context, listen: false)
          .deleteAccount()
          .then((deleted) => {
                if (deleted)
                  {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const TutorialTermsAndConditions()))
                  }
                else
                  {
                    Fluttertoast.showToast(
                        msg: "Une erreur est survenue",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 3,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0)
                  }
              });
    };

    BlurryDialog alert = BlurryDialog(
        "Attention !",
        "La suppression de votre compte entrainera la perte de l'ensemble de vos données ainsi que de tout vos événements. Voulez vous supprimer votre compte malgré tout ?",
        continueCallBack);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
