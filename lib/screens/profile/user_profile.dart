import 'package:ann_see/model/user.dart' as model;
import 'package:ann_see/screens/tutorial/tutorial_terms_conditions.dart';
import 'package:ann_see/viewmodel/account_viewmodel.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar.dart'
    as ann_see;
import 'package:ann_see/widgets/profile/profile_button.dart';
import 'package:ann_see/widgets/registration/text_input_field.dart';
import 'package:ann_see/widgets/utils/blurry_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  final lastNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final model.User currentUser =
        Provider.of<AuthViewModel>(context, listen: false).getCurrentUser()
            as model.User;

    lastNameController.text = currentUser.nom;
    firstNameController.text = currentUser.prenom;
    emailController.text = currentUser.email;

    bool isFormUntouched() {
      return lastNameController.text.toString().trim() == currentUser.nom &&
          firstNameController.text.toString().trim() == currentUser.prenom &&
          emailController.text.toString().trim() == currentUser.email &&
          passwordController.text.toString().trim() == "";
    }

    void saveUserProfileChanges() {
      if (isFormUntouched()) {
        Fluttertoast.showToast(
            msg: "Vous n'avez modifié aucun champ",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else if (passwordController.text.toString().trim() != "" &&
          passwordController.text.length < 6) {
        Fluttertoast.showToast(
            msg: "Le mot de passe doit faire 6 caractères ou plus !",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Provider.of<AccountViewModel>(context, listen: false)
            .updateUserAccount(
                name: lastNameController.text.toString().trim(),
                firstname: firstNameController.text.toString().trim(),
                email: emailController.text.toString().trim(),
                password: passwordController.text.toString().trim())
            .then((updated) {
          if (updated) {
            setState(() {
              Fluttertoast.showToast(
                msg: "Changements sauvegardés !",
                backgroundColor: const Color(0xFF3FC1C9),
                timeInSecForIosWeb: 3,
              );
            });
          } else {
            Fluttertoast.showToast(
                msg: "Une erreur est survenue (relancez l'application)",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 3,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        });
      }
    }

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: Neumorphic(
                    style: const NeumorphicStyle(
                        border: NeumorphicBorder(
                            color: Color(0xFFEEEEEE), width: 3),
                        boxShape: NeumorphicBoxShape.circle(),
                        lightSource: LightSource.topLeft,
                        shadowLightColorEmboss: Color(0xFFFFFFFF),
                        shadowDarkColorEmboss: Color(0xBFAAAACC),
                        intensity: 3,
                        depth: 3),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/images/user_placeholder.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                ),
                Column(
                  children: [
                    TextInputField(
                        controller: lastNameController,
                        hintText: "Dupont",
                        obscureText: false),
                    TextInputField(
                        controller: firstNameController,
                        hintText: "Sophie",
                        obscureText: false),
                    TextInputField(
                        controller: emailController,
                        hintText: "sophie.dupont@gmail.com",
                        obscureText: false),
                    TextInputField(
                        controller: passwordController,
                        hintText: "Modifier votre mot de passe",
                        obscureText: true),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ProfileButton(
                          iconData: Icons.delete_forever_rounded,
                          color: Colors.red,
                          onPressed: deleteUserAccount),
                      ProfileButton(
                          iconData: Icons.save_rounded,
                          color: const Color(0xFF3FC1C9),
                          onPressed: saveUserProfileChanges)
                    ],
                  ),
                ),
                const ann_see.BottomNavigationBar()
              ],
            ),
            Positioned(
              top: 15,
              right: 0,
              child: ProfileButton(
                  iconData: Icons.logout,
                  color: const Color(0xFF3FC1C9),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const TutorialTermsAndConditions()));
                  }),
            ),
          ],
        ),
      ),
    );
  }

  void deleteUserAccount() {
    VoidCallback continueCallBack = () {
      Provider.of<AccountViewModel>(context, listen: false)
          .deleteAccount()
          .then((deleted) => {
                if (deleted)
                  {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const TutorialTermsAndConditions()))
                  }
                else
                  {
                    Fluttertoast.showToast(
                        msg: "Une erreur est survenue",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 3,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0)
                  }
              });
    };

    BlurryDialog alert = BlurryDialog(
        "Attention !",
        "La suppression de votre compte entrainera la perte de l'ensemble de vos données et vous ne pourrez plus accèder au service de l'application. Voulez vous quand même supprimer votre compte ?",
        continueCallBack);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
