import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class TutorialWelcome extends StatelessWidget {
  const TutorialWelcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Text(
                  "👋",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 28,
                      color: Color(0xE6222831),
                      fontWeight: FontWeight.bold,
                      fontFamily: "Rubik"),
                ),
              ),
              const Text(
                "Bienvenue sur ANN'SEE",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 26,
                    color: Color(0xE6222831),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Rubik"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 40),
                child: Neumorphic(
                  style: NeumorphicStyle(
                      shape: NeumorphicShape.concave,
                      boxShape: NeumorphicBoxShape.roundRect(
                          BorderRadius.circular(40)),
                      color: const Color(0xFFEEEEEE),
                      lightSource: LightSource.bottomRight,
                      shadowLightColorEmboss: const Color(0xFFFFFFFF),
                      shadowDarkColorEmboss: const Color(0xBFAAAACC),
                      intensity: 3,
                      depth: 3),
                  child: Container(
                    width: 180,
                    height: 389,
                    decoration: const BoxDecoration(
                      shape: BoxShape.rectangle,
                      image: DecorationImage(
                          image:
                              AssetImage('assets/mock/annsee_mock_welcome.png'),
                          fit: BoxFit.cover),
                    ),
                  ),
                ),
              ),
              const Flexible(
                child: Text(
                  "Swipe les propositions et découvre les activités faîtes pour toi",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xCC222831),
                      fontWeight: FontWeight.normal,
                      fontFamily: "Avenir Next"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
