import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class TutorialActivity extends StatelessWidget {
  const TutorialActivity({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: Text(
                  "🕵️‍♂️",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 28,
                      color: Color(0xE6222831),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Rubik"),
                ),
              ),
              const Text(
                "Découvre les propositions",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 26,
                    color: Color(0xE6222831),
                    fontWeight: FontWeight.w700,
                    fontFamily: "Rubik"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 35, 0, 35),
                child: Neumorphic(
                  style: NeumorphicStyle(
                      shape: NeumorphicShape.concave,
                      boxShape: NeumorphicBoxShape.roundRect(
                          BorderRadius.circular(40)),
                      color: const Color(0xFFEEEEEE),
                      lightSource: LightSource.bottomRight,
                      shadowLightColorEmboss: const Color(0xFFFFFFFF),
                      shadowDarkColorEmboss: const Color(0xBFAAAACC),
                      intensity: 3,
                      depth: 5),
                  child: Container(
                    width: 180,
                    height: 389,
                    decoration: const BoxDecoration(
                      shape: BoxShape.rectangle,
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/mock/annsee_mock_activity.png'),
                          fit: BoxFit.cover),
                    ),
                  ),
                ),
              ),
              const Flexible(
                child: Text(
                  "Explore l'ensemble des propositions, par thème et par catégorie",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xCC222831),
                      fontWeight: FontWeight.w400,
                      fontFamily: "Avenir Next"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
