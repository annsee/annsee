import 'dart:async';

import 'package:ann_see/screens/homepage/homepage.dart';
import 'package:ann_see/screens/login/login.dart';
import 'package:ann_see/screens/register/registration_choice.dart';
import 'package:ann_see/viewmodel/shared_viewmodel.dart';
import 'package:ann_see/widgets/buttons/authentication_button.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_ml_model_downloader/firebase_ml_model_downloader.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class TutorialTermsAndConditions extends StatefulWidget {
  const TutorialTermsAndConditions({Key? key}) : super(key: key);

  @override
  State<TutorialTermsAndConditions> createState() =>
      _TutorialTermsAndConditionsState();
}

class _TutorialTermsAndConditionsState
    extends State<TutorialTermsAndConditions> {
  bool isSwitched = false;

  void authenticateAsAnonymousUserAndSendLoginEvent() async {
    await FirebaseAuth.instance.signInAnonymously();
    await FirebaseAnalytics.instance.logLogin();
  }

  void downloadModel(
      {required String modelName,
      required Future<dynamic> Function() completion}) {
    final FirebaseModelDownloader firebaseModelManager =
        FirebaseModelDownloader.instance;

    firebaseModelManager
        .getModel(
            modelName,
            FirebaseModelDownloadType.localModelUpdateInBackground,
            FirebaseModelDownloadConditions(
                iosAllowsBackgroundDownloading: true,
                iosAllowsCellularAccess: true))
        .then((value) {
          Provider.of<SharedViewModel>(context, listen: false).model =
              value.file;
        })
        .whenComplete(completion)
        .catchError((error, stackTrace) {
          Fluttertoast.showToast(
              msg:
                  "Téléchargement échoué, recommandations indisponibles. Vérifiez votre connexion. ❌",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(40),
        decoration: const BoxDecoration(color: Color(0xFFEEEEEE)),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          "🧙‍♂️️",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 32),
                        ),
                      ),
                      Text(
                        "Plus qu'une étape !",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 26,
                            color: Color(0xE6222831),
                            fontWeight: FontWeight.w700,
                            fontFamily: "Rubik"),
                      ),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 60),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            LimitedBox(
                              maxWidth: 300,
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: const TextSpan(
                                  style: TextStyle(
                                      fontSize: 14, color: Color(0xE6222831)),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            "Les données dont nous disposons sont celles que vous nous fournissez et nous les utilisons uniquement pour le bon fonctionnement de l'application."),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              AuthenticationButton(
                onPressed: goToLogin,
                buttonText: "Connexion",
                color: const Color(0xFF3FC1C9),
              ),
              AuthenticationButton(
                onPressed: goToRegistrationChoice,
                buttonText: "Inscription",
                color: const Color(0xFFFF75A0),
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 60),
                  child: Row(
                    children: [
                      Flexible(
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: const TextSpan(
                            style: TextStyle(
                                fontSize: 14, color: Color(0xE6222831)),
                            children: <TextSpan>[
                              TextSpan(
                                  text: "Vos données sont stockées en ",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Color(0xCC222831),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Avenir Next")),
                              TextSpan(
                                  text: "Europe",
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text:
                                      " et leur confidentialité est soumise à la RGPD 🕵️",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Color(0xCC222831),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Avenir Next"))
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
              /* Padding(
                padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
                child: NeumorphicButton(
                    key: null,
                    onPressed: () {
                      if (isSwitched) {
                        authenticateAsAnonymousUserAndSendLoginEvent();
                        downloadModel(
                            modelName: Provider.of<SharedViewModel>(context,
                                    listen: false)
                                .remoteModelName,
                            completion: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const CulturalProfileGame())));
                        authenticateAsAnonymousUserAndSendLoginEvent();
                        downloadModel(
                            modelName: Provider.of<SharedViewModel>(context,
                                    listen: false)
                                .remoteModelName,
                            completion: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const CulturalProfileGame())));
                        authenticateAsAnonymousUserAndSendLoginEvent();
                        downloadModel(
                            modelName: Provider.of<SharedViewModel>(context,
                                    listen: false)
                                .remoteModelName,
                            completion: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const CulturalProfileGame())));
                      }
                    },
                    padding: const EdgeInsets.fromLTRB(60, 20, 60, 20),
                    style: NeumorphicStyle(
                        disableDepth: !isSwitched,
                        shape: NeumorphicShape.flat,
                        boxShape: NeumorphicBoxShape.roundRect(
                            BorderRadius.circular(25)),
                        color: const Color(0x4DFFFFFF),
                        lightSource: LightSource.topLeft,
                        shadowDarkColor: const Color(0xBFAAAACC),
                        shadowLightColor: const Color(0xFFFFFFFF),
                        intensity: 3,
                        depth: 3),
                    child: const Text(
                      "Continuer",
                      style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF222831),
                          fontWeight: FontWeight.bold,
                          fontFamily: "Rubik"),
                    )),
              )*/
            ],
          ),
        ),
      ),
    );
  }

  void goToRegistrationChoice() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const RegistrationChoice()),
      );
    });
  }

  void goToHomepage() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Homepage()),
      );
    });
  }

  void goToLogin() {
    setState(() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Login()),
      );
    });
  }
}
