/// Flutter icons ShoppingBag
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  ShoppingBag
///      fonts:
///       - asset: fonts/ShoppingBag.ttf
///
/// 
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
///
import 'package:flutter/widgets.dart';

class ShoppingBag {
  ShoppingBag._();

  static const _kFontFam = 'ShoppingBag';
  static const String? _kFontPkg = null;

  static const IconData shopping_bag = IconData(0xf290, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
