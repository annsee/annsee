/// Flutter icons Culture
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  Culture
///      fonts:
///       - asset: fonts/Culture.ttf
///
/// 
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
///
import 'package:flutter/widgets.dart';

class Culture {
  Culture._();

  static const _kFontFam = 'Culture';
  static const String? _kFontPkg = null;

  static const IconData theater_masks = IconData(0xf630, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
