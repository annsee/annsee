import 'package:ann_see/screens/game/cultural_profile_game.dart';
import 'package:ann_see/screens/login/login.dart';
import 'package:ann_see/screens/register/organizer_registration.dart';
import 'package:ann_see/screens/register/registration_choice.dart';
import 'package:ann_see/screens/tutorial/tutorial_activity.dart';
import 'package:ann_see/screens/tutorial/tutorial_map.dart';
import 'package:ann_see/screens/tutorial/tutorial_terms_conditions.dart';
import 'package:ann_see/screens/tutorial/tutorial_welcome.dart';
import 'package:ann_see/locator.dart';
import 'package:ann_see/viewmodel/account_viewmodel.dart';
import 'package:ann_see/viewmodel/activity_viewmodel.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/viewmodel/category_viewmodel.dart';
import 'package:ann_see/viewmodel/game_viewmodel.dart';
import 'package:ann_see/viewmodel/recommandation_viewmodel.dart';
import 'package:ann_see/viewmodel/review_viewmodel.dart';
import 'package:ann_see/viewmodel/shared_viewmodel.dart';
import 'package:ann_see/widgets/tutorial/tutorial_bottom_navigation_bar.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter/services.dart';

void main() async {
  setupLocator();

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final PageController _pageController = PageController(initialPage: 0);
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: AuthViewModel()),
        ChangeNotifierProvider.value(value: AccountViewModel()),
        ChangeNotifierProvider.value(value: CategoryViewModel()),
        ChangeNotifierProvider.value(value: ActivityViewModel()),
        ChangeNotifierProvider.value(value: GameViewModel()),
        ChangeNotifierProvider.value(value: RecommandationViewModel()),
        ChangeNotifierProvider.value(value: ReviewViewModel()),
        ChangeNotifierProvider.value(value: SharedViewModel())
      ],
      child: NeumorphicApp(
        debugShowCheckedModeBanner: false,
        title: 'Ann\'see',
        themeMode: ThemeMode.light,
        theme: const NeumorphicThemeData(
            baseColor: Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            depth: 3,
            intensity: 3),
        darkTheme: const NeumorphicThemeData(
          baseColor: Color(0xFF3E3E3E),
          lightSource: LightSource.topLeft,
          depth: 6,
        ),
        home: Scaffold(
          // body: const Login(),
          body: PageView(
            controller: _pageController,
            onPageChanged: (newIndex) {
              setState(() {
                _currentIndex = newIndex;
              });
            },
            children: const [
              TutorialWelcome(),
              TutorialMap(),
              TutorialActivity(),
              TutorialTermsAndConditions()
            ],
          ),
          bottomNavigationBar: TutorialBottomNavigationBar(
            currentIndex: _currentIndex,
            onTap: (index) {
              setState(() {
                _pageController.animateToPage(index,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.ease);
              });
            },
          ),
        ),
      ),
    );
  }
}
