import 'package:ann_see/locator.dart';
import 'package:ann_see/model/activity.dart';
import 'package:ann_see/service/category_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ActivityService {
  final CollectionReference _activitiesCollectionReference = FirebaseFirestore
      .instance
      .collection("activities")
      .withConverter<Activity>(
          fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
          toFirestore: (activity, _) => activity.toJson());

  final CategoryService _categoryService = locator<CategoryService>();

  List<Activity>? activities;

  Future<bool> create(
      {int? id,
      required DocumentReference category,
      required Timestamp date,
      required String label,
      required String description,
      required int limiteAge,
      required int nbPersonnes,
      required int prix,
      required GeoPoint localisation,
      required String url,
      required DocumentReference organizer}) async {
    try {
      // create a new user profile on firestore
      Activity activity = Activity(
          id: id,
          category: category,
          date: date,
          label: label,
          description: description,
          limiteAge: limiteAge,
          nbPersonnes: nbPersonnes,
          prix: prix,
          localisation: localisation,
          url: url,
          organizer: organizer);

      await _activitiesCollectionReference.doc().set(activity);

      var activityDoc =
          await _activitiesCollectionReference.where("id", isEqualTo: id).get();

      var activityRef =
          _activitiesCollectionReference.doc(activityDoc.docs.first.id);

      await _categoryService.addToCategoryList(category.id, activityRef);

      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<DocumentSnapshot<Object?>> get(String uid) async {
    return await _activitiesCollectionReference.doc(uid).get();
  }

  Future<bool> addToReviewList(String uid, DocumentReference review) async {
    var activity = await get(uid);

    var activityReviews = (activity.data()! as Activity).reviews;

    if (activityReviews != null) {
      activityReviews.add(review);
    } else {
      activityReviews = <DocumentReference>[review];
    }

    await _activitiesCollectionReference
        .doc(uid)
        .update({"reviews": activityReviews});

    return true;
  }
}
