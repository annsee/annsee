import 'dart:ffi';

import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryService {
  final CollectionReference _categoriesCollectionReference = FirebaseFirestore
      .instance
      .collection("categories")
      .withConverter<Category>(
          fromFirestore: (snapshot, _) => Category.fromJson(snapshot.data()!),
          toFirestore: (category, _) => category.toJson());

  List<Category>? categories;

  int categoryIconIndex = 0;
  int categoryColorIndex = 0;

  Future<DocumentSnapshot<Object?>> get(String uid) async {
    return await _categoriesCollectionReference.doc(uid).get();
  }

  Future<bool> addToCategoryList(String uid, DocumentReference activity) async {
    var category = await get(uid);

    var categoryActivities = (category.data()! as Category).activities;

    categoryActivities.add(activity);

    await _categoriesCollectionReference
        .doc(uid)
        .update({"activities": categoryActivities});

    return true;
  }
}
