import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/model/user.dart' as my;
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

class OrganizerService {
  final CollectionReference _organizersCollectionReference = FirebaseFirestore
      .instance
      .collection("organizers")
      .withConverter<Organizer>(
          fromFirestore: (snapshot, _) => Organizer.fromJson(snapshot.data()!),
          toFirestore: (organizer, _) => organizer.toJson());

  Future<bool> create(Organizer organizer) async {
    try {
      await _organizersCollectionReference.doc(organizer.id).set(organizer);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<Organizer?> get(String uid) async {
    try {
      var organizerData = await _organizersCollectionReference.doc(uid).get();
      return organizerData.data() as Organizer;
    } catch (e) {
      return null;
    }
  }

  Future<bool> delete(String uid) async {
    try {
      await _organizersCollectionReference.doc(uid).delete();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> update(
      {required String uid,
      required String name,
      required String email,
      required String phone,
      required String category}) async {
    try {
      await _organizersCollectionReference.doc(uid).update(
          {"nom": name, "mail": email, "phone": phone, "category": category});
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
