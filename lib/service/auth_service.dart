import 'package:ann_see/locator.dart';
import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/model/user.dart' as my;
import 'package:ann_see/service/organizer_service.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:flutter/foundation.dart';
import 'package:ann_see/service/user_service.dart';

class AuthService {
  final firebase.FirebaseAuth _firebaseAuth = firebase.FirebaseAuth.instance;
  final UserService _userFirestoreService = locator<UserService>();
  final OrganizerService _organizerFirestoreService =
      locator<OrganizerService>();

  dynamic _currentUser;
  dynamic get currentUser => _currentUser;

  Future<bool> loginWithEmail({
    required String email,
    required String password,
  }) async {
    try {
      var authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      await populateCurrentUser(authResult.user!);
      await FirebaseAnalytics.instance.logLogin();
      return authResult.user != null;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> userSignUpWithEmail({
    required String email,
    required String password,
    required String nom,
    required String prenom,
    required String genre,
  }) async {
    try {
      var authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // create a new user profile on firestore
      var currentUser = my.User(
        id: authResult.user!.uid,
        email: email,
        nom: nom,
        prenom: prenom,
        genre: genre,
      );

      await _userFirestoreService.create(currentUser);

      await loginWithEmail(email: email, password: password);

      return authResult.user != null;
    } catch (e) {
      return false;
    }
  }

  Future<bool> organizerSignUpWithEmail({
    required String email,
    required String password,
    required String nom,
    required String phone,
    required String category,
  }) async {
    try {
      var authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // create a new user profile on firestore
      var currentOrganizer = Organizer(
        id: authResult.user!.uid,
        nom: nom,
        mail: email,
        phone: phone,
        category: category,
      );

      await _organizerFirestoreService.create(currentOrganizer);

      await loginWithEmail(email: email, password: password);

      return authResult.user != null;
    } catch (e) {
      return false;
    }
  }

  Future<firebase.User> isUserLoggedIn() async {
    var user = _firebaseAuth.currentUser;
    await populateCurrentUser(user!);
    return user;
  }

  Future populateCurrentUser(firebase.User user) async {
    dynamic tempUser;
    tempUser = await _userFirestoreService.get(user.uid);
    if (tempUser != null) {
      _currentUser = tempUser;
    } else {
      _currentUser = await _organizerFirestoreService.get(user.uid);
    }
  }

  bool setCurrentUserEmpty() {
    _currentUser = null;
    return true;
  }
}
