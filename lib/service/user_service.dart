import 'package:ann_see/model/user.dart' as my;
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

class UserService {
  final CollectionReference _usersCollectionReference =
      FirebaseFirestore.instance.collection("users").withConverter<my.User>(
          fromFirestore: (snapshot, _) => my.User.fromJson(snapshot.data()!),
          toFirestore: (user, _) => user.toJson());

  Future<bool> create(my.User user) async {
    try {
      await _usersCollectionReference.doc(user.id).set(user);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<my.User?> get(String uid) async {
    try {
      var userData = await _usersCollectionReference.doc(uid).get();
      return userData.data() as my.User;
    } catch (e) {
      return null;
    }
  }

  Future<bool> delete(String uid) async {
    try {
      await _usersCollectionReference.doc(uid).delete();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> update(
      {required String uid,
      required String name,
      required String firstname,
      required String email}) async {
    try {
      await _usersCollectionReference
          .doc(uid)
          .update({"nom": name, "prenom": firstname, "email": email});
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
