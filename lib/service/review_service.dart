import 'package:ann_see/locator.dart';
import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/review.dart';
import 'package:ann_see/service/activity_service.dart';
import 'package:ann_see/service/category_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ReviewService {
  final CollectionReference _reviewsCollectionReference =
      FirebaseFirestore.instance.collection("reviews").withConverter<Review>(
          fromFirestore: (snapshot, _) => Review.fromJson(snapshot.data()!),
          toFirestore: (review, _) => review.toJson());

  final CollectionReference _activitiesCollectionReference = FirebaseFirestore
      .instance
      .collection('activities')
      .withConverter<Activity>(
          fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
          toFirestore: (activity, _) => activity.toJson());

  final ActivityService _activityService = locator<ActivityService>();

  Future<dynamic> create(
      {int? id,
      required int note,
      required String comment,
      required DocumentReference user,
      required Activity activity}) async {
    try {
      var reviews = await _reviewsCollectionReference.get();

      var reviewsSize = reviews.size;
      // create a new user profile on firestore
      Review review =
          Review(id: reviewsSize + 1, note: note, comment: comment, user: user);

      await _reviewsCollectionReference.doc().set(review);

      var reviewDoc = await _reviewsCollectionReference
          .where("id", isEqualTo: review.id)
          .get();

      var activityDoc = await _activitiesCollectionReference
          .where("id", isEqualTo: activity.id)
          .get();

      var activityRef =
          _activitiesCollectionReference.doc(activityDoc.docs.first.id);

      var reviewRef = _reviewsCollectionReference.doc(reviewDoc.docs.first.id);

      await _activityService.addToReviewList(
          activityDoc.docs.first.id, reviewRef);

      var activityUpdated = await activityRef.get();

      return activityUpdated.data() as Activity;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
