import 'package:ann_see/locator.dart';
import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/model/user.dart' as my;
import 'package:ann_see/service/auth_service.dart';
import 'package:ann_see/service/organizer_service.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:flutter/foundation.dart';
import 'package:ann_see/service/user_service.dart';

class AccountService {
  final firebase.FirebaseAuth _firebaseAuth = firebase.FirebaseAuth.instance;
  final AuthService _authService = locator<AuthService>();
  final UserService _userService = locator<UserService>();
  final OrganizerService _organizerService = locator<OrganizerService>();

  Future<bool> deleteAccount() async {
    try {
      await _firebaseAuth.currentUser?.delete();

      await _userService.delete(_authService.currentUser.id);
      await _organizerService.delete(_authService.currentUser.id);

      _authService.setCurrentUserEmpty();

      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> updateUserAccount(
      {required String name,
      required String firstname,
      required String email,
      required String password}) async {
    try {
      if (email != _firebaseAuth.currentUser?.email) {
        await _firebaseAuth.currentUser?.updateEmail(email);
      }

      if (password != "") {
        await _firebaseAuth.currentUser?.updatePassword(password);
      }

      await _userService.update(
        uid: _firebaseAuth.currentUser!.uid,
        name: name,
        firstname: firstname,
        email: email,
      );

      await _authService.populateCurrentUser(_firebaseAuth.currentUser!);

      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> updateOrganizerAccount(
      {required String name,
      required String email,
      required String phone,
      required String category,
      required String password}) async {
    try {
      if (email != _firebaseAuth.currentUser?.email) {
        await _firebaseAuth.currentUser?.updateEmail(email);
      }

      if (password != "") {
        await _firebaseAuth.currentUser?.updatePassword(password);
      }

      await _organizerService.update(
          uid: _firebaseAuth.currentUser!.uid,
          name: name,
          email: email,
          phone: phone,
          category: category);

      await _authService.populateCurrentUser(_firebaseAuth.currentUser!);

      return true;
    } catch (e) {
      return false;
    }
  }
}
