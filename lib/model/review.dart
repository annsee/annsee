import 'package:cloud_firestore/cloud_firestore.dart';

class Review {
  Review(
      {required this.id,
      required this.note,
      required this.comment,
      required this.user});

  Review.fromJson(Map<String, dynamic> json)
      : this(
            id: json["id"],
            note: json['note'],
            comment: json['comment'],
            user: json['user']);

  final int id;
  final int note;
  final String comment;
  final DocumentReference user;

  Map<String, dynamic> toJson() {
    return {'id': id, 'note': note, 'comment': comment, 'user': user};
  }
}
