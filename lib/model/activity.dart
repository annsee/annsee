import 'package:cloud_firestore/cloud_firestore.dart';

class Activity {
  Activity(
      {this.id,
      required this.category,
      required this.date,
      required this.label,
      required this.description,
      required this.limiteAge,
      required this.nbPersonnes,
      required this.prix,
      required this.localisation,
      required this.url,
      this.organizer,
      this.reviews});

  Activity.fromJson(Map<String, dynamic> json)
      : this(
            id: json["id"],
            category: json['category'],
            date: json['date'],
            label: json['label'],
            description: json['description'],
            limiteAge: json['limiteAge'],
            nbPersonnes: json['nbPersonnes'],
            prix: json['prix'],
            localisation: json['localisation'],
            url: json['URL'],
            organizer: json['organizer'],
            reviews: json["reviews"]);

  final int? id;
  final DocumentReference category;
  final Timestamp date;
  final String label;
  final String description;
  final int limiteAge;
  final int nbPersonnes;
  final int prix;
  final GeoPoint localisation;
  final String url;
  final DocumentReference? organizer;
  final List<dynamic>? reviews;

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'category': category,
      'date': date,
      'label': label,
      'description': description,
      'limiteAge': limiteAge,
      'nbPersonnes': nbPersonnes,
      'prix': prix,
      'localisation': localisation,
      'URL': url,
      'organizer': organizer,
      'reviews': reviews
    };
  }
}
