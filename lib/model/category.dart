import 'package:cloud_firestore/cloud_firestore.dart';

class Category {
  Category(
      {this.uid,
      required this.id,
      required this.label,
      required this.description,
      required this.url,
      required this.icon,
      required this.color,
      required this.activities});

  Category.fromJson(Map<String, dynamic> json)
      : this(
            uid: json["uid"],
            id: json["id"],
            label: json['label'],
            description: json['description'],
            url: json['URL'],
            icon: json['icon'],
            color: json['color'],
            activities: json['activities']);

  final String? uid;
  final int id;
  final String label;
  final String description;
  final String url;
  final String icon;
  final String color;
  final List<dynamic> activities;

  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'id': id,
      'label': label,
      'description': description,
      'url': url,
      'icon': icon,
      'color': color,
      'activities': activities
    };
  }
}
