class Organizer {
  Organizer(
      {required this.id,
      required this.nom,
      required this.mail,
      required this.phone,
      required this.category});

  Organizer.fromJson(Map<String, dynamic> json)
      : this(
            id: json['id'],
            nom: json['nom'],
            mail: json['mail'],
            phone: json['phone'],
            category: json['category']);

  final String id;
  final String nom;
  final String mail;
  final String phone;
  final String category;

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nom': nom,
      'mail': mail,
      'phone': phone,
      'category': category
    };
  }
}
