class User {
  User(
      {required this.id,
      required this.email,
      required this.nom,
      required this.prenom,
      required this.genre});

  User.fromJson(Map<String, dynamic> json)
      : this(
            id: json['id'],
            email: json["email"],
            nom: json['nom'],
            prenom: json['prenom'],
            genre: json['genre']);

  final String id;
  final String email;
  final String nom;
  final String prenom;
  final String genre;

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'nom': nom,
      'prenom': prenom,
      'genre': genre,
    };
  }
}
