import 'package:ann_see/service/account_service.dart';
import 'package:ann_see/service/activity_service.dart';
import 'package:ann_see/service/auth_service.dart';
import 'package:ann_see/service/category_service.dart';
import 'package:ann_see/service/navigation_bar_service.dart';
import 'package:ann_see/service/organizer_service.dart';
import 'package:ann_see/service/review_service.dart';
import 'package:ann_see/service/user_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => OrganizerService());
  locator.registerLazySingleton(() => AccountService());
  locator.registerLazySingleton(() => CategoryService());
  locator.registerLazySingleton(() => ActivityService());
  locator.registerLazySingleton(() => ReviewService());
  locator.registerLazySingleton(() => NavigationBarService());
}
