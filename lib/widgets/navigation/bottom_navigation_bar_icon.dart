import 'package:ann_see/locator.dart';
import 'package:ann_see/service/navigation_bar_service.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class NavigationBarIcon extends StatefulWidget {
  final IconData iconData;
  final Widget routeToGo;
  final bool isCenter;
  final int index;

  const NavigationBarIcon(
      {Key? key,
      required this.iconData,
      required this.routeToGo,
      required this.isCenter,
      required this.index})
      : super(key: key);

  @override
  State<NavigationBarIcon> createState() => _NavigationBarIconState();
}

class _NavigationBarIconState extends State<NavigationBarIcon> {
  final NavigationBarService _navigationBarService =
      locator<NavigationBarService>();

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
        onPressed: () {
          setState(() {
            _navigationBarService.index = widget.index;
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => widget.routeToGo),
            );
          });
        },
        style: const NeumorphicStyle(
          disableDepth: true,
        ),
        child: NeumorphicIcon(
          widget.iconData,
          size: widget.isCenter ? 40 : 22,
          style: widget.index == _navigationBarService.index
              // see if there's a better way to only change color
              ? const NeumorphicStyle(
                  color: Color(0xFF3FC1C9),
                  shape: NeumorphicShape.flat,
                  depth: 1,
                  intensity: 3,
                  lightSource: LightSource.topLeft,
                )
              : const NeumorphicStyle(
                  color: Color(0x80222831),
                  shape: NeumorphicShape.flat,
                  depth: 1,
                  intensity: 3,
                  lightSource: LightSource.topLeft,
                ),
        ));
  }
}
