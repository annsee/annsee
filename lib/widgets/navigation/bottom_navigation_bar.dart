import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/organizer.dart';
import 'package:ann_see/model/user.dart' as model;
import 'package:ann_see/screens/category/category.dart';
import 'package:ann_see/screens/event/event_list.dart';
import 'package:ann_see/screens/event/new_event.dart';
import 'package:ann_see/screens/homepage/homepage.dart';
import 'package:ann_see/screens/game/cultural_profile_game.dart';
import 'package:ann_see/screens/profile/organizer_profile.dart';
import 'package:ann_see/screens/profile/user_profile.dart';
import 'package:ann_see/screens/recommandations/recommendation.dart';
import 'package:ann_see/viewmodel/auth_viewmodel.dart';
import 'package:ann_see/widgets/navigation/bottom_navigation_bar_icon.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:provider/provider.dart';

class BottomNavigationBar extends StatefulWidget {
  const BottomNavigationBar({
    Key? key,
  }) : super(key: key);

  @override
  State<BottomNavigationBar> createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<BottomNavigationBar> {
  var organizerRef = FirebaseFirestore.instance
      .collection("organizers")
      .withConverter<Organizer>(
          fromFirestore: (snapshot, _) => Organizer.fromJson(snapshot.data()!),
          toFirestore: (organizer, _) => organizer.toJson())
      .doc(firebase.FirebaseAuth.instance.currentUser!.uid);

  late var activitiesStream = FirebaseFirestore.instance
      .collection("activities")
      .withConverter<Activity>(
          fromFirestore: (snapshot, _) => Activity.fromJson(snapshot.data()!),
          toFirestore: (activity, _) => activity.toJson())
      .where("organizer", isEqualTo: organizerRef)
      .snapshots();

  @override
  Widget build(BuildContext context) {
    return Neumorphic(
      style: NeumorphicStyle(
          shape: NeumorphicShape.flat,
          boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
          depth: 3,
          intensity: 3,
          lightSource: LightSource.topLeft,
          color: const Color(0xFFEEEEEE)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: (Provider.of<AuthViewModel>(context, listen: false)
                .getCurrentUser() is model.User)
            ? [
                const NavigationBarIcon(
                  iconData: Icons.thumbs_up_down,
                  routeToGo: CulturalProfileGame(),
                  isCenter: false,
                  index: 0,
                ),
                NavigationBarIcon(
                    iconData: Icons.person,
                    routeToGo:
                        Provider.of<AuthViewModel>(context, listen: false)
                                .getCurrentUser() is Organizer
                            ? const OrganizerProfile()
                            : const UserProfile(),
                    isCenter: false,
                    index: 1),
                const NavigationBarIcon(
                  iconData: Icons.home,
                  routeToGo: Homepage(),
                  isCenter: true,
                  index: 2,
                ),
                const NavigationBarIcon(
                  iconData: Icons.category,
                  routeToGo: Category(),
                  isCenter: false,
                  index: 3,
                ),
                const NavigationBarIcon(
                  iconData: Icons.local_fire_department_rounded,
                  routeToGo: Recommendation(),
                  isCenter: false,
                  index: 4,
                ),
              ]
            : [
                const NavigationBarIcon(
                    iconData: Icons.person,
                    routeToGo: OrganizerProfile(),
                    isCenter: false,
                    index: 0),
                StreamBuilder<QuerySnapshot>(
                  stream: activitiesStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return const Text("Something went wrong");
                    }

                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Text("Loading...");
                    }

                    if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.active) {
                      var organizerHasActivity = snapshot.data!.docs.where(
                          (DocumentSnapshot snapshot) =>
                              (snapshot.data() as Activity).organizer ==
                              organizerRef);

                      return organizerHasActivity.isNotEmpty
                          ? const NavigationBarIcon(
                              iconData: Icons.list,
                              routeToGo: EventList(),
                              isCenter: true,
                              index: 1)
                          : const NavigationBarIcon(
                              iconData: Icons.add,
                              routeToGo: NewEvent(
                                backArrow: false,
                              ),
                              isCenter: true,
                              index: 1);
                    } else {
                      return const NavigationBarIcon(
                          iconData: Icons.add,
                          routeToGo: NewEvent(
                            backArrow: false,
                          ),
                          isCenter: true,
                          index: 1);
                    }
                  },
                ),
                const NavigationBarIcon(
                  iconData: Icons.category,
                  routeToGo: Category(),
                  isCenter: false,
                  index: 2,
                ),
              ],
      ),
    );
  }
}
