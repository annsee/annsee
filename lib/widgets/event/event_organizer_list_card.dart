import 'package:ann_see/model/activity.dart';
import 'package:ann_see/model/category.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class EventOrganizerListCard extends StatefulWidget {
  final Activity activity;
  final Category category;

  const EventOrganizerListCard(
      {Key? key, required this.activity, required this.category})
      : super(key: key);

  @override
  State<EventOrganizerListCard> createState() => _EventOrganizerListCardState();
}

class _EventOrganizerListCardState extends State<EventOrganizerListCard> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 200,
        width: MediaQuery.of(context).size.width / 1.1,
        child: NeumorphicButton(
            onPressed: () {},
            style: const NeumorphicStyle(
              disableDepth: true,
            ),
            child: Stack(
              fit: StackFit.expand,
              children: [
                ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(widget.activity.url,
                      fit: BoxFit.cover,
                      height: 200,
                      width: MediaQuery.of(context).size.width),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.activity.label,
                          style: const TextStyle(
                              fontSize: 40,
                              color: Color(0xE6FFFFFF),
                              fontWeight: FontWeight.w800,
                              fontFamily: "Rubik"),
                        ),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 60),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        NeumorphicButton(
                          key: null,
                          onPressed: () {},
                          style: const NeumorphicStyle(
                              shape: NeumorphicShape.concave,
                              boxShape: NeumorphicBoxShape.circle(),
                              color: Color(0xFFDDDDDD),
                              lightSource: LightSource.topLeft,
                              shadowDarkColor: Color(0xBFAAAACC),
                              shadowLightColor: Color(0xFFFFFFFF),
                              intensity: 2,
                              depth: 2),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              NeumorphicIcon(
                                Icons.edit,
                                size: 35,
                                style: const NeumorphicStyle(
                                    color: Color(0xFF3FC1C9),
                                    lightSource: LightSource.topLeft,
                                    depth: 2,
                                    intensity: 2),
                              ),
                            ],
                          ),
                        ),
                        NeumorphicButton(
                          key: null,
                          onPressed: () {},
                          style: const NeumorphicStyle(
                              shape: NeumorphicShape.concave,
                              boxShape: NeumorphicBoxShape.circle(),
                              color: Color(0xFFDDDDDD),
                              lightSource: LightSource.topLeft,
                              shadowDarkColor: Color(0xBFAAAACC),
                              shadowLightColor: Color(0xFFFFFFFF),
                              intensity: 2,
                              depth: 2),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              NeumorphicIcon(
                                Icons.delete,
                                size: 35,
                                style: const NeumorphicStyle(
                                    color: Color(0xFFFF75A0),
                                    lightSource: LightSource.topLeft,
                                    depth: 2,
                                    intensity: 2),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ],
            )));
  }
}
