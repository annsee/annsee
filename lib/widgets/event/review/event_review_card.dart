import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class EventReviewCard extends StatelessWidget {
  final String userPseudo;
  final String userReview;
  final double userNote;

  const EventReviewCard({
    required this.userPseudo,
    required this.userReview,
    required this.userNote,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //TODO: Ajouter une date sous le pseudo, en italic et comme la catégorie
    //TODO: Ajouter un bouton pour ajouter un évènement
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Neumorphic(
        style: NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
            color: const Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: const Color(0xBFAAAACC),
            shadowLightColor: const Color(0xFFFFFFFF),
            intensity: 3,
            depth: 3),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 15),
                      child: Text(
                        userPseudo,
                        style: const TextStyle(
                            fontSize: 18,
                            color: Color(0xE6222831),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Avenir Next"),
                      ),
                    ),
                    // TODO: https://pub.dev/documentation/flutter_rating_bar/latest/flutter_rating_bar/RatingBar-class.html
                    RatingBar.builder(
                      initialRating: userNote,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 25,
                      itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => const Icon(
                        Icons.star,
                        color: Color(0xFF3FC1C9),
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    )
                  ],
                ),
              ),
              Text(
                userReview,
                style: const TextStyle(
                    fontSize: 14,
                    color: Color(0xE6222831),
                    fontWeight: FontWeight.w200,
                    fontFamily: "Avenir Next"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
