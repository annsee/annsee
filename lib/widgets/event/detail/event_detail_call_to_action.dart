import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class EventDetailCallToAction extends StatelessWidget {
  final String actionText;
  final IconData iconData;
  final dynamic onPressed;
  final Color color;

  const EventDetailCallToAction({
    Key? key,
    required this.actionText,
    required this.iconData,
    required this.onPressed,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 40, 0),
      child: NeumorphicButton(
        key: null,
        onPressed: onPressed,
        padding: const EdgeInsets.all(20),
        style: NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
            color: const Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: const Color(0xBFAAAACC),
            shadowLightColor: const Color(0xFFFFFFFF),
            intensity: 3,
            depth: 3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: NeumorphicIcon(
                iconData,
                size: 40,
                style: NeumorphicStyle(
                    color: color,
                    lightSource: LightSource.topLeft,
                    depth: 1,
                    intensity: 3),
              ),
            ),
            Text(
              actionText.toUpperCase(),
              style: const TextStyle(
                  fontSize: 14,
                  color: Color(0xD9222831),
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0.5,
                  fontFamily: "Avenir Next"),
            ),
          ],
        ),
      ),
    );
  }
}
