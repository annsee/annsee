import 'dart:ui';
import 'package:flutter/material.dart';

// Merci de pas me blame pour ce code dégueulasse, j'ai passé ma journée
// ça vient de S.O et c'est fonctionnel et j'ai pas le temps !

class BlurryDialog extends StatefulWidget {
  String title;
  String content;
  VoidCallback continueCallBack;

  BlurryDialog(this.title, this.content, this.continueCallBack, {Key? key})
      : super(key: key);

  @override
  State<BlurryDialog> createState() => _BlurryDialogState();
}

class _BlurryDialogState extends State<BlurryDialog> {
  TextStyle textStyle = const TextStyle(color: Colors.black);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child: AlertDialog(
          title: Text(
            widget.title,
            style: textStyle,
          ),
          content: Text(
            widget.content,
            style: textStyle,
          ),
          actions: <Widget>[
            TextButton(
              child: const Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text("Continue"),
              onPressed: () {
                widget.continueCallBack();
              },
            ),
          ],
        ));
  }
}
