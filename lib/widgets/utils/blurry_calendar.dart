import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class BlurryCalendar extends StatefulWidget {
  dynamic onSelectionChanged;
  VoidCallback continueCallBack;

  BlurryCalendar(this.onSelectionChanged, this.continueCallBack, {Key? key})
      : super(key: key);

  @override
  State<BlurryCalendar> createState() => _BlurryCalendarState();
}

class _BlurryCalendarState extends State<BlurryCalendar> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child: AlertDialog(
          title: const Text("Sélectionner la date de l'événement"),
          content: SizedBox(
            width: 300,
            height: 300,
            child: SfDateRangePicker(
              onSelectionChanged: widget.onSelectionChanged,
              selectionMode: DateRangePickerSelectionMode.range,
              initialSelectedRange: PickerDateRange(
                  DateTime.now().subtract(const Duration(days: 4)),
                  DateTime.now().add(const Duration(days: 3))),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text("Valider"),
              onPressed: () {
                widget.continueCallBack();
              },
            ),
          ],
        ));
  }
}
