import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:transparent_image/transparent_image.dart';

class RecommendedCard extends StatelessWidget {
  final String image;
  final String label;
  final dynamic onPressed;

  const RecommendedCard(
      {Key? key,
      required this.image,
      required this.label,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: NeumorphicButton(
          onPressed: onPressed,
          style: const NeumorphicStyle(
            disableDepth: true,
          ),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(8.0),
                  child: CachedNetworkImage(
                      imageUrl: image,
                      placeholder: (context, url) =>
                          Image.memory(kTransparentImage),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error, size: 200),
                      fit: BoxFit.cover,
                      height: 100,
                      width: MediaQuery.of(context).size.width)),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Text(
                  label,
                  style: const TextStyle(
                      fontSize: 28,
                      color: Color(0xFFFFFFFF),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Rubik"),
                ),
              ),
            ],
          )),
    );
  }
}
