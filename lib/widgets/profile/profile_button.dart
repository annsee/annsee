import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class ProfileButton extends StatelessWidget {
  final IconData iconData;
  final Color color;
  final dynamic onPressed;

  const ProfileButton({
    Key? key,
    required this.iconData,
    required this.color,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
      onPressed: onPressed,
      style: const NeumorphicStyle(
          shape: NeumorphicShape.flat,
          boxShape: NeumorphicBoxShape.circle(),
          color: Color(0xFFEEEEEE),
          lightSource: LightSource.topLeft,
          shadowDarkColor: Color(0xBFAAAACC),
          shadowLightColor: Color(0xFFFFFFFF),
          intensity: 3,
          depth: 2),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: NeumorphicIcon(
          iconData,
          size: 35,
          style: NeumorphicStyle(
              depth: 2,
              intensity: 3,
              color: color
          ),
        ),
      ),
    );
  }
}
