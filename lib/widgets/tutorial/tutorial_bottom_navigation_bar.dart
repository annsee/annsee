import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class TutorialBottomNavigationBar extends StatelessWidget {
  final int currentIndex;
  final dynamic onTap;

  const TutorialBottomNavigationBar({
    required this.currentIndex,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
      child: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.circle), label: ""),
          BottomNavigationBarItem(icon: Icon(Icons.circle), label: ""),
          BottomNavigationBarItem(icon: Icon(Icons.circle), label: ""),
          BottomNavigationBarItem(icon: Icon(Icons.circle), label: ""),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: onTap,
        iconSize: 7,
        elevation: 0,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: const Color(0xFF00ADB5),
        unselectedItemColor: const Color(0x80222831),
        backgroundColor: Colors.transparent,
      ),
    );
  }
}
