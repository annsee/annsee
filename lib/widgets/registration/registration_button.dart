import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class RegistrationButton extends StatelessWidget {
  final dynamic onPressed;

  const RegistrationButton({Key? key, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
        onPressed: onPressed,
        padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
        style: NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(25)),
            color: const Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: const Color(0xBFAAAACC),
            shadowLightColor: const Color(0xFFFFFFFF),
            intensity: 5,
            depth: 5),
        child: const Text(
          "S'inscrire !",
          style: TextStyle(
              fontSize: 18,
              color: Color(0xFF3FC1C9),
              fontWeight: FontWeight.w700,
              fontFamily: "Rubik"),
        ));
  }
}
