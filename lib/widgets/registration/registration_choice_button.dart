import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class RegistrationChoiceButton extends StatelessWidget {
  final Color color;
  final dynamic onPressed;
  final IconData iconData;

  const RegistrationChoiceButton(
      {Key? key,
      required this.color,
      required this.onPressed,
      required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
      onPressed: onPressed,
      style: const NeumorphicStyle(
          shape: NeumorphicShape.convex,
          boxShape: NeumorphicBoxShape.circle(),
          color: Color(0xFFEEEEEE),
          lightSource: LightSource.topLeft,
          shadowDarkColor: Color(0xBFAAAACC),
          shadowLightColor: Color(0xFFFFFFFF),
          intensity: 3,
          depth: 3),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: NeumorphicIcon(
          iconData,
          size: 70,
          style: NeumorphicStyle(
              color: color,
              lightSource: LightSource.topLeft,
              depth: 3,
              intensity: 3),
        ),
      ),
    );
  }
}
