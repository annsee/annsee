import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class TextInputField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final bool obscureText;
  final TextInputType inputType;
  final int multilineRows;
  final bool enabled;

  const TextInputField(
      {Key? key,
      required this.controller,
      required this.hintText,
      required this.obscureText,
      this.inputType = TextInputType.text,
      this.multilineRows = 1,
      this.enabled = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Neumorphic(
        style: NeumorphicStyle(
            disableDepth: !enabled,
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(25)),
            color: const Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowLightColorEmboss: const Color(0xFFFFFFFF),
            shadowDarkColorEmboss: const Color(0xBFAAAACC),
            intensity: 3,
            depth: -3),
        margin: const EdgeInsets.fromLTRB(0, 0, 0, 5),
        child: TextField(
          enabled: enabled,
          keyboardType: inputType,
          minLines: 1,
          maxLines: multilineRows,
          controller: controller,
          obscureText: obscureText,
          textAlign: TextAlign.start,
          decoration: InputDecoration(
              hintText: hintText,
              hintStyle: const TextStyle(color: Color(0x4D222831)),
              contentPadding: const EdgeInsets.symmetric(horizontal: 20),
              border: InputBorder.none),
          style: const TextStyle(
              fontSize: 14.0,
              color: Color(0xFF222831),
              fontWeight: FontWeight.w200,
              fontFamily: "Avenir Next"),
        ),
      ),
    );
  }
}
