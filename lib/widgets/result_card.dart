import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class ResultCard extends StatelessWidget {
  final int rank;
  final String image;
  final dynamic category;
  final dynamic onPressed;

  const ResultCard(
      {Key? key,
      required this.rank,
      required this.image,
      required this.category,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: NeumorphicButton(
          onPressed: onPressed,
          style: const NeumorphicStyle(
            disableDepth: true,
          ),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              ClipRRect(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(8.0),
                child: Image.asset(image,
                    fit: BoxFit.cover,
                    height: 100,
                    width: MediaQuery.of(context).size.width),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Text(
                  "#$rank $category",
                  style: const TextStyle(
                      fontSize: 32,
                      color: Color(0xE6FFFFFF),
                      fontWeight: FontWeight.w700,
                      fontFamily: "Rubik"),
                ),
              ),
            ],
          )),
    );
  }
}
