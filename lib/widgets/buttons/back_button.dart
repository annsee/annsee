import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class BackButton extends StatelessWidget {
  final dynamic onPressed;

  const BackButton({
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 40, 0),
      child: NeumorphicButton(
        onPressed: onPressed,
        style: const NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.circle(),
            color: Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: Color(0xBFAAAACC),
            shadowLightColor: Color(0xFFFFFFFF),
            intensity: 2,
            depth: 2),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: NeumorphicIcon(
                Icons.arrow_back,
                size: 30,
                style: const NeumorphicStyle(
                    color: Color(0xE6222831),
                    lightSource: LightSource.topLeft,
                    depth: 2,
                    intensity: 2),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
