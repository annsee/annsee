import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class AuthenticationButton extends StatelessWidget {
  final dynamic onPressed;
  final String buttonText;
  final Color color;

  const AuthenticationButton({
    Key? key,
    required this.onPressed,
    required this.buttonText,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: NeumorphicButton(
          onPressed: onPressed,
          padding: const EdgeInsets.fromLTRB(70, 20, 70, 20),
          style: NeumorphicStyle(
              shape: NeumorphicShape.flat,
              boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(25)),
              color: color,
              depth: 3,
              intensity: 3,
              lightSource: LightSource.topLeft,
              shadowDarkColor: const Color(0xBFAAAACC),
              shadowLightColor: const Color(0xFFFFFFFF)),
          child: Text(
            buttonText,
            style: const TextStyle(
                fontSize: 22,
                color: Color(0xFFEEEEEE),
                fontWeight: FontWeight.w400),
          )),
    );
  }
}
