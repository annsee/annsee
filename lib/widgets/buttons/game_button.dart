import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class GameButton extends StatelessWidget {
  final IconData iconData;
  final Color color;
  final dynamic onPressed;

  const GameButton(
      {Key? key,
      required this.color,
      required this.onPressed,
      required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: NeumorphicButton(
        key: null,
        onPressed: onPressed,
        style: const NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.circle(),
            color: Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: Color(0xBFAAAACC),
            shadowLightColor: Color(0xFFFFFFFF),
            intensity: 2,
            depth: 2),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: NeumorphicIcon(
                iconData,
                size: 50,
                style: NeumorphicStyle(
                    color: color,
                    lightSource: LightSource.topLeft,
                    depth: 2,
                    intensity: 2),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
