import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class CategoryDetailCard extends StatelessWidget {
  final String title;
  final String description;
  final dynamic onPressed;

  const CategoryDetailCard({
    required this.title,
    required this.description,
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(40, 0, 40, 20),
      child: SizedBox(
        height: 100,
        child: NeumorphicButton(
          style: NeumorphicStyle(
              shape: NeumorphicShape.flat,
              boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
              depth: 1,
              intensity: 3,
              lightSource: LightSource.topLeft,
              shadowDarkColor: const Color(0xBFAAAACC),
              shadowLightColor: const Color(0xFFFFFFFF),
              color: const Color(0xFFEEEEEE)),
          onPressed: onPressed,
          child: Row(
            children: [
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Text(
                        title,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 16,
                            color: Color(0xE6222831),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Rubik"),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                          child: Text(
                            description,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                fontSize: 14,
                                color: Color(0xE6222831),
                                fontWeight: FontWeight.normal,
                                fontFamily: "Avenir Next"),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              NeumorphicIcon(
                Icons.arrow_forward,
                style: const NeumorphicStyle(
                  depth: 2,
                  intensity: 2,
                  lightSource: LightSource.topLeft,
                  shadowDarkColor: Color(0xBFAAAACC),
                  shadowLightColor: Color(0xFFFFFFFF),
                  color: Color(0xE6222831),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
