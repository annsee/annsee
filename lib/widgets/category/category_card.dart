import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class CategoryCard extends StatelessWidget {
  final IconData iconData;
  final dynamic onPressed;
  final String title;
  final Color color;

  const CategoryCard({
    required this.iconData,
    required this.onPressed,
    required this.title,
    required this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: NeumorphicButton(
        key: null,
        onPressed: onPressed,
        padding: const EdgeInsets.all(20),
        style: NeumorphicStyle(
            shape: NeumorphicShape.flat,
            boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
            color: const Color(0xFFEEEEEE),
            lightSource: LightSource.topLeft,
            shadowDarkColor: const Color(0xBFAAAACC),
            shadowLightColor: const Color(0xFFFFFFFF),
            intensity: 3,
            depth: 3),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
              child: NeumorphicIcon(
                iconData,
                size: 40,
                style: NeumorphicStyle(
                    color: color,
                    lightSource: LightSource.topLeft,
                    depth: 2,
                    intensity: 2),
              ),
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  fontSize: 14,
                  color: Color(0xD9222831),
                  fontWeight: FontWeight.bold,
                  fontFamily: "Rubik"),
            ),
          ],
        ),
      ),
    );
  }
}
