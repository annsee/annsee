import 'dart:io';

import 'package:ann_see/presentation/compass_icons.dart';
import 'package:ann_see/presentation/culture_icons.dart';
import 'package:ann_see/presentation/event_icons.dart';
import 'package:ann_see/presentation/food_icons.dart';
import 'package:ann_see/presentation/heritage_icons.dart';
import 'package:ann_see/presentation/leaf_icons.dart';
import 'package:ann_see/presentation/night_club_icons.dart';
import 'package:ann_see/presentation/relaxation_icons.dart';
import 'package:ann_see/presentation/shopping_bag_icons.dart';
import 'package:ann_see/presentation/sport_icons.dart';
import 'package:flutter/cupertino.dart';

class SharedViewModel with ChangeNotifier {
  final Map<String, dynamic> icons = {
    "event": Event.user_friends,
    "nightclub": NightClub.emo_sunglasses,
    "heritage": Heritage.church,
    "culture": Culture.theater_masks,
    "sport": Sport.fitness_center,
    "compass": Compass.drafting_compass,
    "shoppingbag": ShoppingBag.shopping_bag,
    "leaf": Leaf.leaf,
    "relaxation": Relaxation.umbrella_beach,
    "food": Food.food,
    "default": Culture.theater_masks
  };

  final Map<String, Color> colors = {
    "cyan": const Color(0xFF3FC1C9),
    "pink": const Color(0xFFFF75A0),
    "yellow": const Color(0xFFFCE38A),
    "lightyellow": const Color(0xFFEAFFD0),
    "lightblue": const Color(0xFF95E1D3),
    "red": const Color(0xFFFC4A35),
    "magenta": const Color(0xFFC9539E),
    "lime": const Color(0xFF8BC34A),
    "lightpink": const Color(0xFFE2B5DE),
    "purple": const Color(0xFF9891FF),
    "default": const Color(0x00000000)
  };

  late final File model;
  final String defaultLocalModelPath = "";
  final String remoteModelName = "recommendations";
  final int defaultInputLength = 10;
  final int defaultOutputLength = 10;
  final int defaultTopK = 10;
  final int padID = 0;
  final int defaultOutputIDsIndex = 1;
  final int defaultOutputScoresIndex = 0;
  final int defaultLikedListSize = 100;
}
