import 'package:ann_see/locator.dart';
import 'package:ann_see/service/account_service.dart';
import 'package:ann_see/service/auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class AccountViewModel with ChangeNotifier {
  final AccountService _accountService = locator<AccountService>();

  Future<bool> deleteAccount() async {
    notifyListeners();
    return _accountService.deleteAccount();
  }

  Future<bool> updateUserAccount(
      {required String name,
      required String firstname,
      required String email,
      required String password}) async {
    notifyListeners();
    return _accountService.updateUserAccount(
        name: name, firstname: firstname, email: email, password: password);
  }

  Future<bool> updateOrganizerAccount(
      {required String name,
      required String email,
      required String phone,
      required String category,
      required String password}) async {
    notifyListeners();
    return _accountService.updateOrganizerAccount(
        name: name,
        email: email,
        phone: phone,
        category: category,
        password: password);
  }
}
