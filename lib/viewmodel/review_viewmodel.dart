import 'package:ann_see/locator.dart';
import 'package:ann_see/model/activity.dart';
import 'package:ann_see/service/activity_service.dart';
import 'package:ann_see/service/review_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class ReviewViewModel with ChangeNotifier {
  final ReviewService _reviewService = locator<ReviewService>();

  Future<dynamic> create(
      {int? id,
      required int note,
      required String comment,
      required DocumentReference user,
      required Activity activity}) async {
    notifyListeners();
    return _reviewService.create(
        id: id, note: note, comment: comment, user: user, activity: activity);
  }
}
