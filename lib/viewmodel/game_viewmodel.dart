import 'package:ann_see/model/activity.dart';
import 'package:flutter/cupertino.dart';

class GameViewModel with ChangeNotifier {
  int gameCount = 1;

  Map<String, List<Activity>> likes = {
    "0I12MJ8woN5xZhAvKvsn": [],
    "3ImKjX3ILvusz2MEYDkX": [],
    "9Z4d5zQBHSZFtrQI7PQM": [],
    "IQ0f6L3UnuCsGRnFaiwj": [],
    "SsuyCLIEF69aXXz9W3Ag": [],
    "TkOHgppbsef3zaHJSAqW": [],
    "V450a8jPUDiIw1vqYDjQ": [],
    "cR8jE7AuNH9iU8avvvOA": [],
    "hhj6wvsSm3CL23AAtgDQ": [],
    "qQj5dPpogthMUMcOKSPo": [],
  };

  List<String> imagesCategory = [
    "assets/images/city.jpeg",
    "assets/images/mountain.png",
    "assets/images/outdoor.png",
    'assets/images/event.png',
    'assets/images/nature.png'
  ];
}
