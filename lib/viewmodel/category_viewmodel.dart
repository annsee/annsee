import 'dart:math';

import 'package:ann_see/locator.dart';
import 'package:ann_see/model/category.dart';
import 'package:ann_see/presentation/compass_icons.dart';
import 'package:ann_see/presentation/culture_icons.dart';
import 'package:ann_see/presentation/event_icons.dart';
import 'package:ann_see/presentation/heritage_icons.dart';
import 'package:ann_see/presentation/leaf_icons.dart';
import 'package:ann_see/presentation/night_club_icons.dart';
import 'package:ann_see/presentation/shopping_bag_icons.dart';
import 'package:ann_see/presentation/sport_icons.dart';
import 'package:ann_see/service/category_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class CategoryViewModel with ChangeNotifier {
  final CategoryService _categoryService = locator<CategoryService>();

  Future<DocumentSnapshot<Object?>> get({required String uid}) async {
    return _categoryService.get(uid);
  }

  List<Category>? getCategories() {
    return _categoryService.categories;
  }

  List<Category>? setCategories(List<Category> list) {
    _categoryService.categories = list;
    return _categoryService.categories;
  }
}
