import 'package:ann_see/locator.dart';
import 'package:ann_see/service/auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase;

import 'package:flutter/cupertino.dart';

class AuthViewModel with ChangeNotifier {
  final AuthService _authService = locator<AuthService>();

  Future<bool> userRegister(
      {required String email,
      required String password,
      required String nom,
      required String prenom,
      required String genre}) async {
    notifyListeners();
    return _authService.userSignUpWithEmail(
        email: email,
        password: password,
        nom: nom,
        prenom: prenom,
        genre: genre);
  }

  Future<bool> organizerRegister(
      {required String email,
      required String password,
      required String nom,
      required String phone,
      required String category}) async {
    notifyListeners();
    return _authService.organizerSignUpWithEmail(
        email: email,
        password: password,
        nom: nom,
        phone: phone,
        category: category);
  }

  Future<bool> signIn({required String email, required String password}) async {
    notifyListeners();
    return _authService.loginWithEmail(email: email, password: password);
  }

  dynamic getCurrentUser() {
    return _authService.currentUser;
  }
}
