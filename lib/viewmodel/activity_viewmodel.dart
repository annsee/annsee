import 'package:ann_see/locator.dart';
import 'package:ann_see/model/activity.dart';
import 'package:ann_see/service/activity_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class ActivityViewModel with ChangeNotifier {
  final ActivityService _activityService = locator<ActivityService>();

  Future<bool> create(
      {int? id,
      required DocumentReference category,
      required Timestamp date,
      required String label,
      required String description,
      required int limiteAge,
      required int nbPersonnes,
      required int prix,
      required GeoPoint localisation,
      required String url,
      required DocumentReference organizer}) async {
    notifyListeners();
    return _activityService.create(
        id: id,
        category: category,
        date: date,
        label: label,
        description: description,
        limiteAge: limiteAge,
        nbPersonnes: nbPersonnes,
        prix: prix,
        localisation: localisation,
        url: url,
        organizer: organizer);
  }

  List<Activity>? getActivities() {
    return _activityService.activities;
  }

  List<Activity>? setActivities(List<Activity> list) {
    _activityService.activities = list;
    return _activityService.activities;
  }
}
