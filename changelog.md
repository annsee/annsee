# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2022-03-24
### Added
- Intégration de l'identité visuelle et création des écrans (ANNSEE-15) [@Valentin].
- Implémentation de la navigation avec la barre de navigation (ANNSEE-19) [@Valentin].
- Implémentation du design pattern MVVM [@Jérémy].
- Intégration de la base de données Firestore [@Jérémy].
- Intégration de la carte interactive sur la page d'accueil [@Valentin].
- Mise en place du jeu de like avec suivi des activités likées [@Jérémy].
- Création et intégration du tutoriel lors de la première connexion [@Valentin].
- Intégration des activités likés à la carte interactive [@Jérémy].
- Implémentation liste des résultats des likes sur la page d'accueil [@Valentin].
- Intégration catégories et activités globales liés à ces dernières [@Valentin].